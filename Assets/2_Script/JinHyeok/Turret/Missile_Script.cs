﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile_Script : MonoBehaviour
{
    private float attackDamage;
    private Coroutine coroutine;

    public void Init_Func()
    {

    }
    public void Shoot_Func(float _attackDamage, float _power)
    {
        this.gameObject.SetActive(true);
        attackDamage = _attackDamage;

        coroutine = StartCoroutine(Shoot_Cor(_power));
    }
    IEnumerator Shoot_Cor(float _power)
    {
        Transform _thisTrf = this.transform;
        float _x;
        float _y;

        WaitForFixedUpdate _delay = new WaitForFixedUpdate();

        while (true)
        {
            _thisTrf.Translate(Vector3.up * _power * 0.01f, Space.Self);
            _x = _thisTrf.position.x;
            _y = _thisTrf.position.y;
            
            if (50 < _x || -50 > _x || 50 < _y || -50 > _y)
            {
                break;
            }
            else
            {

            }

            yield return _delay;
        }

        Destroy_Func();
        //this.Destroy_Func();
    }
    

    private void Destroy_Func()
    {
        if (coroutine != null)
            StopCoroutine(coroutine);
        this.transform.parent = TurretSystem_Manager.instance.missileGroup_Invisible.transform;
        this.gameObject.SetActive(false);
        //GameObject.Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            Enemy_Script _enemyClass = collision.gameObject.GetComponent<Enemy_Script>();
            _enemyClass.Damaged_Func(this.attackDamage);
            this.Destroy_Func();
        }
        else
        {

        }
    }
}
