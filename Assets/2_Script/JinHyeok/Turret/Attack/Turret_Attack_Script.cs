﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;

public class Turret_Attack_Script : Turret1_Script, IWhichOne
{
    public float attackValue;
    public float attackInterval;
    public float shootPower;
    public GameObject missileObj;
    
    public override void StageEnter_Func()
    {
        base.StageEnter_Func();

        StartCoroutine(Attack_Cor());
    }

    IEnumerator Attack_Cor()
    {
        while (true)
        {
            GameObject _missileObj = GameObject.Instantiate(this.missileObj, this.transform.position, this.transform.rotation);
            Missile_Script _missileClass = _missileObj.GetComponent<Missile_Script>();
            _missileClass.Init_Func();
            _missileClass.Shoot_Func(this.attackValue, shootPower);

            yield return new WaitForSeconds(this.attackInterval);
        }
    }

    public void SelectCancel_Func()
    {

    }

    public void Selected_Func(bool _repeat = false)
    {

    }
}
