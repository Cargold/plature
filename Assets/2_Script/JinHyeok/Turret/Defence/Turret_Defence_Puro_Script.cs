﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Turret_Defence_Puro_Script : MonoBehaviour
{
    public Image energyImg;
    public Image recoverImg;

    public void Init_Func()
	{
        this.SetEnergy_Func(1f);
        this.SetRecover_Func(1f);
    }

    public void SetEnergy_Func(float _value)
    {
        energyImg.fillAmount = _value;
    }
    public void SetRecover_Func(float _value)
    {
        recoverImg.fillAmount = _value;
    }
}
