﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenceTest_Script : MonoBehaviour {

    public static DefenceTest_Script instance;

    private void Awake()
    {
        if (DefenceTest_Script.instance == null)
            DefenceTest_Script.instance = this;
    }

    private PivotSystem_Script pivotClass;
    [SerializeField]private DefenceAttackTest_Script attackTest;

    public void Init_Func()
    {
        pivotClass = new PivotSystem_Script(this.transform);
        attackTest.Init_Func(DefenceType.notShoot , 3);
    }

    public void Move_Func(float _angle)
    {
        pivotClass.SetPos_Func(new Vector2(Plature_Manager.Instance.Return_Pivot().position.x, Plature_Manager.Instance.Return_Pivot().position.y + 3));
        pivotClass.SetRev_Func(_angle);
    }
}
public enum DefenceType
{
    Shoot = 0,
    notShoot
}