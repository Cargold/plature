﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenceAttack_Morning : DefenceAttackTest_Script
{
    public override void Attack()
    {
        StartCoroutine(MovePos());
    }

    IEnumerator MovePos()
    {
        while(true)
        {
            Transform pivotTrf = Plature_Manager.Instance.Return_Pivot();
            Vector3 vec = DefenceTest_Script.instance.transform.position;
            float angle = Mathf.Atan2(vec.y - pivotTrf.position.y, vec.x - pivotTrf.position.x) * 180 / Mathf.PI;
            if (angle < 0)
                angle += 360;

            //Debug.Log(angle);

            int check = 1;
            
            if (angle >180)
                check = 1;
            else
                check = -1;
           ChangeGravity(check);
            yield return new WaitForFixedUpdate();
        }
    }

    public void ChangeGravity(int check)
    {
        Rigidbody2D[] gravity = transform.parent.GetComponentsInChildren<Rigidbody2D>();
        for (int i = 0; i < gravity.Length; i++)
        {
            gravity[i].gravityScale = check;
        }
    }
}
