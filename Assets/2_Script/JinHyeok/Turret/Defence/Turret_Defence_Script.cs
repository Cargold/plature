﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_Defence_Script : Turret1_Script
{
    //public Turret_Defence_Puro_Script puroClass;

    public float energy_Max;
    public float energy_Recent;
    public float energy_damaged;
    public float recoverDelay_Condition;
    public float recoverDelay_Recent;
    public float recoverValue;

    public override void Init_Func()
    {
        base.Init_Func();

        energy_Max = energy_Recent;

        //puroClass.Init_Func();
    }

    public override void StageEnter_Func()
    {
        base.StageEnter_Func();

        StartCoroutine(Recover_Cor());
    }
    IEnumerator Recover_Cor()
    {
        WaitForFixedUpdate _delay = new WaitForFixedUpdate();

        while(true)
        {
            // 에너지가 최대치가 아니라면
            if(energy_Recent < energy_Max)
            {
                // 회복 조건에 충족하지 못한다면
                if(recoverDelay_Recent < recoverDelay_Condition)
                {
                    recoverDelay_Recent += 0.02f;
                   // puroClass.SetRecover_Func(recoverDelay_Recent / recoverDelay_Condition);
                }
                else
                {
                    float _recoverValue_Calc = recoverValue * 0.02f;

                    // 회복해도 최대치를 넘지 않는다면
                    if(energy_Recent + _recoverValue_Calc <= energy_Max)
                    {
                        energy_Recent += _recoverValue_Calc;

                        //puroClass.SetEnergy_Func(energy_Recent / energy_Max);
                    }
                    else
                    {
                        energy_Recent = energy_Max;

                        //puroClass.SetEnergy_Func(energy_Recent / energy_Max);
                    }
                }
            }
            else
            {

            }

            yield return _delay;
        }
    }

    public virtual void CollideEnemy_Func(Enemy_Script _enemyClass)
    {
        if(energy_damaged <= energy_Recent)
        {
            energy_Recent -= energy_damaged;

            recoverDelay_Recent = 0;
            //puroClass.SetRecover_Func(recoverDelay_Recent / recoverDelay_Condition);

            //puroClass.SetEnergy_Func(energy_Recent / energy_Max);

            Vector2 _colPos = _enemyClass.transform.position;
            _enemyClass.Die_Func();
        }
        else
        {

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            Enemy_Script _enemyClass = collision.gameObject.GetComponent<Enemy_Script>();
            
            this.CollideEnemy_Func(_enemyClass);
        }
        else
        {

        }
    }
}
