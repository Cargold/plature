﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenceAttackTest_Script : MonoBehaviour {
    private DefenceType type;
    protected float coolTime;
    protected float presentTime;
    public virtual void Init_Func(DefenceType _type, float _coolTime = 0f)
    {
        type = _type;
        coolTime = _coolTime;
        presentTime = 0f;
        if (type == DefenceType.notShoot)
            GetComponent<Collider2D>().isTrigger = true;
        else if (type == DefenceType.Shoot)
            Attack();
    }

    public virtual void Attack()
    {

    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            Enemy_Script enemy = col.GetComponent<Enemy_Script>();
            if (enemy != null)
                Attack(enemy);
        }
            
    }

    public void Attack(Enemy_Script enemy)
    {
        enemy.Die_Func();
    }
}
