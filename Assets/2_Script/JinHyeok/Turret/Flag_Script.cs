﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag_Script : MonoBehaviour {

    private PivotSystem_Script pivotClass;
    public static Flag_Script instance; 
    public float angle;

    public void Init_Func()
    {
        if (Flag_Script.instance == null)
            Flag_Script.instance = this;
        pivotClass = new PivotSystem_Script(this.transform);
    }

    public void Move_Func(float _angle)
    {
        angle = _angle;
        pivotClass.SetPos_Func(new Vector2(Plature_Manager.Instance.Return_Pivot().position.x, Plature_Manager.Instance.Return_Pivot().position.y + 3));
        pivotClass.SetRev_Func(angle);
    }
}
