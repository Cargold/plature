﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_Script : MonoBehaviour
{
    [SerializeField]private float speed;
    private float attackDemage;
    private float destination;
    private float destinationPresent;
    private Coroutine coroutineMove;

    private PivotSystem_Script pivotSystemClass;
    private Transform pivotTrf;
    private int check;

    private Coroutine coroutineAttack;
    [SerializeField] private SpriteRenderer turretImg;
    public float attackValue;       //공격력
    public float attackSpownTime;   //공격 쿨타임
    public float shootPower;        //날리는 속도
    public float randomRange;
    public float maxRange;
    public GameObject missileObj;

    public virtual void Init(TurretType type)
    {
        //피봇에 대한 초기화
        pivotTrf = Plature_Manager.Instance.Return_Pivot();
        pivotSystemClass = new PivotSystem_Script(this.transform);
 
        //터렛 데이터 값을 DB로부터 받아와서 _turretData에 저장
        Turret_Data _turretData = DataBase_Manager.Instance.ReturnTurretData(type);

        //터렛 본연의 값 초기화
        speed = _turretData.turretSpeed;
        turretImg.sprite = _turretData.turretImg;
        maxRange = _turretData.tureetRange;

        //미사일에 대한 정보값 초기화
        attackValue = _turretData.turretDamage;
        attackSpownTime = _turretData.turretSpownTime;
        shootPower = _turretData.turretShootSpeed;

        //터렛의 목적지를 초기화
        check = 1;
        destination = Flag_Script.instance.angle + 90;
        randomRange = Random.Range(maxRange - 5f, maxRange + 5f);
        
        destinationPresent = destination + check * randomRange;
    }
    public void SetPos(Vector2 vec)
    {
        pivotSystemClass.SetPos_Func(vec);
    }

    public void SetDestination(float _angle)
    {
        destination = _angle;
        destinationPresent = destination + check * randomRange;
    }

    public void Move_Func()
    {
        coroutineMove = StartCoroutine(Move());
        coroutineAttack = StartCoroutine(Attack_Cor());
    }

    public virtual void Attack_Func()
    {
        
    }

    private IEnumerator Move()
    {
        while (true)
        {
            float angle = Mathf.Atan2(transform.position.y - pivotTrf.position.y, transform.position.x - pivotTrf.position.x) * 180 / Mathf.PI;
            if (angle < 0)
                angle += 360;
            if (destinationPresent < 0)
                destinationPresent = 360 + destinationPresent;
            else if (destinationPresent > 360)
                destinationPresent = destinationPresent - 360;
            if (angle < destinationPresent - 1f)
            {
                if (angle < 90 && destinationPresent - 1f > 270)
                    pivotSystemClass.Move_Func(DirectionType.Right, speed * 5 * Time.deltaTime);
                else
                    pivotSystemClass.Move_Func(DirectionType.Left, speed * 5 * Time.deltaTime);
            }
                
            else if (angle >= destinationPresent + 1f)
            {
                if (angle > 270 && destinationPresent - 1f < 90)
                    pivotSystemClass.Move_Func(DirectionType.Left, speed * 5 * Time.deltaTime);
                else
                    pivotSystemClass.Move_Func(DirectionType.Right, speed * 5 * Time.deltaTime);
            }
                
            else
            {
                check *= -1;
                destinationPresent = destination + check * randomRange;
            }
            yield return new WaitForFixedUpdate();
        }
    }

    protected virtual IEnumerator Attack_Cor()
    {
        while (true)
        {
            GameObject _missileObj;
            GameObject invisible = TurretSystem_Manager.instance.missileGroup_Invisible;
            if (invisible.transform.childCount > 0)
            {
                _missileObj = invisible.transform.GetChild(0).gameObject;
            }
            else
            {
                _missileObj = GameObject.Instantiate(this.missileObj, this.transform.position, this.transform.rotation);
            }
            _missileObj.transform.parent = TurretSystem_Manager.instance.missileGroup_Visible.transform;
            Missile_Script _missileClass = _missileObj.GetComponent<Missile_Script>();
            _missileClass.Init_Func();
            _missileClass.Shoot_Func(this.attackValue, shootPower);

            yield return new WaitForSeconds(this.attackSpownTime);
        }
    }

    public void StopCor_Turret()
    {
        if (coroutineMove != null)
            StopCoroutine(coroutineMove);
        if (coroutineAttack != null)
            StopCoroutine(coroutineAttack);
    }
}

[System.Serializable]
public struct Turret_Data
{
    public TurretType id;
    public Sprite turretImg;
    public float turretSpeed;
    public float tureetRange;

    public float turretDamage;
    public float turretShootSpeed;
    public float turretSpownTime;
}

public interface ReturnTurretDB
{
    Turret_Data ReturnTurretData(TurretType turretDB);
}