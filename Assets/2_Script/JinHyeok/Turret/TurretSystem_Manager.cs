﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSystem_Manager : MonoBehaviour
{
    public static TurretSystem_Manager instance;

    [SerializeField] private List<Turret_Script> turretList;
    [SerializeField] private GameObject turretInfo;
    [SerializeField] private SpriteRenderer flag;
    public GameObject missileGroup_Invisible;
    public GameObject missileGroup_Visible;
    public void Init_Func()
    {
        TurretSystem_Manager.instance = this;
        
        turretList = new List<Turret_Script>();
        missileGroup_Invisible = new GameObject();
        missileGroup_Invisible.name = "Missile_Group_Invisible";
        missileGroup_Visible = new GameObject();
        missileGroup_Visible.name = "Missile_Group_Visible";
    }

    public void TestAdd()
    {
        AddTurret(TurretType.Rifle);
    }

    public void AddTurretList(Turret_Script turretAdd)
    {
        turretList.Add(turretAdd);
        turretAdd.Move_Func();
    }

    public void AddTurret(TurretType type)
    {
        GameObject turret = Instantiate(turretInfo , transform.position, Quaternion.identity);
        Turret_Script turretScript = turret.GetComponent<Turret_Script>();
        turretScript.Init(type);
        turretScript.SetPos(new Vector2(transform.position.x, transform.position.y + 3.0f));
        AddTurretList(turretScript);
    }

    public void Move(float angle)
    {
        
        for (int i = 0; i < turretList.Count; i++)
        {
            turretList[i].SetDestination(angle);
        }
    }

    public void DestrotTower()
    {
        for (int i = 0; i < turretList.Count; i++)
        {
            turretList[i].StopCor_Turret();
            Destroy(turretList[i]);
        }
        turretList.Clear();
    }
}
