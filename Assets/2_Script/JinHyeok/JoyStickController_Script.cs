﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoyStickController_Script : MonoBehaviour
{
    public Transform Stick;         // 조이스틱.
    public bool isLeft;
    
    private Vector3 StickFirstPos;  // 조이스틱의 처음 위치.
    private Vector3 JoyVec;         // 조이스틱의 벡터(방향)
    private float Radius;           // 조이스틱 배경의 반 지름.
    [SerializeField] private RectTransform canvas;

    public void Init_Func()
    {
        Radius = GetComponent<RectTransform>().sizeDelta.y * 0.5f;
        StickFirstPos = Stick.transform.position;

        // 캔버스 크기에대한 반지름 조절.
        float Can = canvas.localScale.x;
        Radius *= Can;
    }

    // 드래그
    public void Drag(BaseEventData _Data)
    {
        PointerEventData Data = _Data as PointerEventData;
        Vector3 Pos = Data.position;

        //Debug.Log("Pos : " + Pos);
        
        float world_X = Screen.width;
        //Debug.Log("world_X : " + world_X);

        // 조이스틱을 이동시킬 방향을 구함.(오른쪽,왼쪽,위,아래)
        JoyVec = (Pos - StickFirstPos).normalized;

        // 조이스틱의 처음 위치와 현재 내가 터치하고있는 위치의 거리를 구한다.
        float Dis = Vector3.Distance(Pos, StickFirstPos);

        // 거리가 반지름보다 작으면 조이스틱을 현재 터치하고 있는곳으로 이동. 
        if (Dis < Radius)
            Stick.position = StickFirstPos + JoyVec * Dis;
        // 거리가 반지름보다 커지면 조이스틱을 반지름의 크기만큼만 이동.
        else
            Stick.position = StickFirstPos + JoyVec * Radius;

        //해당 조이스틱의 각도를 계산
        float angle = Mathf.Atan2(Pos.y - StickFirstPos.y, Pos.x - StickFirstPos.x) * 180 / Mathf.PI;
        if (angle < 0)
            angle += 360;

        //왼쪽 조이스틱을 컨트롤 할 경우
        if (Pos.x <= world_X / 2 && isLeft == true)
        {
            //PlatureSystem_Manager.Instance.MoveTurret_Func(TurretType.Defence, angle + 90);
            Plature_Manager.Instance.MoveDefence(angle - 90);
        }
        //우측 조이스틱을 컨트롤 할 경우
        else if(Pos.x > world_X / 2 && isLeft == false)
        {
            //PlatureSystem_Manager.Instance.MoveTurret_Func(TurretType.Attack, angle + 90);
            Plature_Manager.Instance.MoveTurret(angle);
        }
       
    }

    // 드래그 끝.
    public void DragEnd()
    {
        Stick.position = StickFirstPos; // 스틱을 원래의 위치로.
        JoyVec = Vector3.zero;          // 방향을 0으로.
    }
}
