﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;
using UnityEngine.UI;

public class PlatureSystem_Manager : MonoBehaviour
{
    public static PlatureSystem_Manager Instance;

    public Transform pivotGroupTrf;
    public List<Transform> pivotTrfList;
    public Transform turretGroupTrf;

    public float healthPoint_Max;
    public float healthPoint_Recent;
    public Image healthImg;

    public SpriteRenderer body;

    public Turret_Defence_Script turretDefenceClass;
    public Turret_Attack_Script[] turretAttackClassArr;
    
    public TapGroup_Script tapGroupClass;

    private int pivotNum;

    public GameObject uiCanvasObj;
    
    public void Init_Func()
    {
        Instance = this;

        pivotTrfList = new List<Transform>();

        for (int i = 0; i < 10; i++)
        {
            Transform _pivotTrf = this.GeneratePivot_Func(true);

            pivotTrfList.Add(_pivotTrf);
        }

        turretDefenceClass.Init_Func();
        for (int i = 0; i < turretAttackClassArr.Length; i++)
        {
            turretAttackClassArr[i].Init_Func();
        }

        tapGroupClass.Init_Func(3, this.turretAttackClassArr);

        healthPoint_Max = healthPoint_Recent;

        uiCanvasObj.SetActive(false);
    }

    public void StageEnter_Func()
    {
        turretDefenceClass.StageEnter_Func();
        for (int i = 0; i < turretAttackClassArr.Length; i++)
        {
            turretAttackClassArr[i].StageEnter_Func();
        }

        uiCanvasObj.SetActive(true);
    }
    
    public Transform GetPivot_Func()
    {
        if(0 < this.pivotTrfList.Count)
        {
            Transform _pivotTrf = pivotTrfList[0];
            pivotTrfList.Remove(_pivotTrf);

            _pivotTrf.SetParent(null);

            return _pivotTrf;
        }
        else
        {
            return this.GeneratePivot_Func(false);
        }
    }
    public void ReturnPivot_Func(Transform _pivotTrf)
    {
        if (this.pivotTrfList.Contains(_pivotTrf) == false)
        {
            _pivotTrf.SetParent(pivotGroupTrf);
            pivotTrfList.Add(_pivotTrf);
        }
        else
        {

        }
        

        Debug.Log("Return : " + _pivotTrf.name);
    }
    private void Update()
    {
        for (int i = 0; i < this.pivotTrfList.Count; i++)
        {
            Transform _pivotTrf = this.pivotTrfList[i];

            if (0 < _pivotTrf.childCount)
            {
                Debug.LogError("_pivotTrf.childCount : " + _pivotTrf.childCount);
                Debug.LogError("Name : " + _pivotTrf.name);
            }
        }
    }

    private Transform GeneratePivot_Func(bool _isInit)
    {
        GameObject _pivotObj = new GameObject("Pivot_" + pivotNum++);

        if(_isInit == true)
        {
            _pivotObj.transform.SetParent(pivotGroupTrf);
        }
        else
        {
            
        }
        
        _pivotObj.transform.localPosition = Vector3.zero;

        return _pivotObj.transform;
    }

    #region Turret Group
    public void AddTurret_Func(Turret_Attack_Script _attackTurretClass)
    {

    }
    #endregion

    public void MoveTurret_Func(TurretType _turretType, float _angle)
    {
        if(_turretType == TurretType.Attack)
        {
            Turret_Attack_Script _attackTurretClass = this.SelectedTurret;
            _attackTurretClass.Move_Func(_angle);
        }
        else if(_turretType == TurretType.Defence)
        {
            turretDefenceClass.Move_Func(_angle);
        }
        else
        {

        }
    }

    public void Damaged_Func(float _damageValue)
    {
        if (0f < this.healthPoint_Recent - _damageValue)
        {
            this.healthPoint_Recent -= _damageValue;

            healthImg.fillAmount = this.healthPoint_Recent / this.healthPoint_Max;

            StopCoroutine("Damaged_Cor");
            StartCoroutine("Damaged_Cor");
        }
        else
        {
            this.healthPoint_Recent = 0f;

            healthImg.fillAmount = 0f;

            this.Die_Func();
        }
    }
    IEnumerator Damaged_Cor()
    {
        body.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        body.color = Color.white;
    }

    public void Die_Func()
    {
        Application.LoadLevel(0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            Enemy_Script _enemyClass = collision.gameObject.GetComponent<Enemy_Script>();
            this.Damaged_Func(_enemyClass.healthPoint);
            _enemyClass.Die_Func();
        }
        else
        {

        }
    }
    public Turret_Attack_Script SelectedTurret { get { return this.tapGroupClass.GetSelectedTurret_Func(); } }
}
