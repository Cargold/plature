﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plature_Manager : MonoBehaviour, IGameState
{
    public static Plature_Manager Instance;

    [SerializeField] private DefenceTest_Script defenceManager;
    [SerializeField] private TurretSystem_Manager turretManager;
    [SerializeField] private Flag_Script flagManager;

    [SerializeField] private Plature_Data platureData;

    private Coroutine coroutine;

    public SpriteRenderer platureImg;
    public int level;
    public float hp_max;
    public string platureName { get { return this.platureData.platureID; } }

    public void Init_Func()
    {
        Instance = this;

        turretManager.Init_Func();
        defenceManager.Init_Func();
        flagManager.Init_Func();

        Plature_Script _platureClass = PlayerSystem_Manager.Instance.GetSelectedPlature_Func();
        this.SetPlature_Func(_platureClass);
    }

    public void SetPlature_Func(Plature_Script _platureClass)
    {
        level = _platureClass.level;
        hp_max = _platureClass.hp_max;
        platureImg.sprite = _platureClass.bodyImg;
    }

    public void StageEnter_Func()
    {
        
    }
    
    public void MoveDefence(float angle)
    {
        defenceManager.Move_Func(angle);
    }

    public void MoveTurret(float angle)
    {
        flagManager.Move_Func(angle - 90);
        turretManager.Move(angle);
    }
    

    public Transform Return_Pivot()
    {
        return this.transform;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            if (coroutine != null)
                StopCoroutine(coroutine);
            coroutine = StartCoroutine(Damaged_Cor());
            Enemy_Script _enemyClass = collision.gameObject.GetComponent<Enemy_Script>();
            this.Damaged_Func(_enemyClass.healthPoint);
            _enemyClass.Die_Func();
        }
        else
        {

        }
    }

    private void Damaged_Func(float damage)
    {
        this.hp_max -= damage;
        if (hp_max <= 0)
            Die_Func();
        
    }

    public void Die_Func()
    {
        Application.LoadLevel(0);
    }

    public void StageFinish_Func()
    {

    }

    public void LobbyEnter_Func()
    {

    }

    IEnumerator Damaged_Cor()
    {
        platureImg.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        platureImg.color = Color.white;
    }
}

public interface ISelectedPlature
{
    Plature_Script GetSelectedPlature_Func();
}