﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;

public class Plature_Script : IWhichOne
{
    private Plature_Data platureData;
    public string platureName { get { return this.platureData.platureID; } }
    public int level;
    public int hp_max;
    public Sprite bodyImg;

    public Plature_Script(Plature_Data _platureData, int _level)
    {
        this.platureData = _platureData;
        this.hp_max = _platureData.hp_max + (_platureData.hp_increase * _level);
        this.bodyImg = _platureData.bodySprite;
    }

    public void Selected_Func(bool _repeat = false)
    {

    }

    public void SelectCancel_Func()
    {

    }
}