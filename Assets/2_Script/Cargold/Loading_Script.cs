﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Loading_Script : MonoBehaviour
{
    public Text loadingTxt;
    public bool isDone;

	public void Init_Func()
	{
        this.gameObject.SetActive(true);

        isDone = false;
    }

    public void LoadingDone_Func()
    {
        isDone = true;

        loadingTxt.text = "로딩 끗!";
    }

    public void CallBtn_Func()
    {
        if(isDone == false)
        {

        }
        else
        {
            GameSystem_Manager.Instance.CallBtn_Func("LoadingClear");

            GameObject.Destroy(this.gameObject);
        }
    }
}
