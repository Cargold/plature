﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plature_Data_Prefab : MonoBehaviour
{
    public Plature_Data platureData;
}

[System.Serializable]
public struct Plature_Data
{
    public string platureID;
    public Sprite bodySprite;
    public Sprite topSprite;
    public int hp_max;
    public int hp_increase;
}