﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageSystem_Puro_Manager : MonoBehaviour
{
    public Text stageTxt;
    public Text timeTxt;
    public Text scoreTxt;
    public Text goldTxt;

	public void Init_Func()
	{
        stageTxt.text = "";
        timeTxt.text = "";
        scoreTxt.text = "";
        goldTxt.text = "";
    }
    public void SetStage_Func(int _stage)
    {
        stageTxt.text = _stage.ToString();
    }
    public void SetTime_Func(int _second)
    {
        int _minute = _second / 60;
        _second -= _minute * 60;

        timeTxt.text = string.Format("{0}:{1}", _minute, _second);
    }
    public void SetScore_Func(int _score)
    {
        this.scoreTxt.text = _score.ToString();
    }
    public void SetGold_Func(int _gold)
    {
        this.goldTxt.text = _gold.ToString();
    }
}
