﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameSystem_Manager : MonoBehaviour
{
    public static GameSystem_Manager Instance;
    
    public Loading_Script loadingClass;
    
    private void Awake()
    {
        Instance = this;

        loadingClass.Init_Func();
        
        FindObjectOfType<DataBase_Manager>().Init_Func();
        FindObjectOfType<PlayerSystem_Manager>().Init_Func();
        FindObjectOfType<LobbySystem_Manager>().Init_Func();
        
        FindObjectOfType<StageSystem_Manager>().Init_Func();
        FindObjectOfType<CameraSystem_Manager>().Init_Func();

        loadingClass.LoadingDone_Func();
    }

    public void GameStart_Func()
    {
        StageSystem_Manager.Instance.StageEnter_Func();
        CameraSystem_Manager.Instance.GameStart_Func();
    }

    public void CallBtn_Func(string _key)
    {
        switch (_key)
        {
            case "LoadingClear":
                LobbySystem_Manager.Instance.Enter_Func(LobbyState.Main);
                break;
        }
    }
}
