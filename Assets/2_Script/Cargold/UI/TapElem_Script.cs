﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;

public class TapElem_Script : MonoBehaviour, IWhichOne
{
    [SerializeField] private TapElem_Puro_Script puroClass;
    private TapGroup_Script groupClass;
    [SerializeField] private TapElemState tapElemState;
    public Turret_Attack_Script attackTurretClass;

	public void Init_Func(TapGroup_Script _groupClass, TapElemState _tapElemState, Turret_Attack_Script _attackTurretClass)
	{
        groupClass = _groupClass;

        tapElemState = _tapElemState;

        attackTurretClass = _attackTurretClass;
        if(_attackTurretClass == null)
        {
            puroClass.Init_Func();
        }
        else
        {
            puroClass.Init_Func(null, "1", "Test");
        }
    }
    
    private void CallBtn_Select_Func()
    {
        switch (this.tapElemState)
        {
            case TapElemState.None:
                break;
            case TapElemState.WaitTurret:
                break;
            case TapElemState.SetTurret:
                this.groupClass.Selected_Func(this);
                break;
            case TapElemState.Disable:
                break;
            default:
                break;
        }
    }

    public void Selected_Func(bool _repeat = false)
    {
        puroClass.Selected_Func(true);

        this.attackTurretClass.Selected_Func();
    }

    public void SelectCancel_Func()
    {
        puroClass.Selected_Func(false);
    }

    public void CallBtn_Func(string _key)
    {
        switch (_key)
        {
            case "Click":
                this.CallBtn_Select_Func();
                break;
        }
    }

    public enum TapElemState
    {
        None = -1,
        WaitTurret,
        SetTurret,
        Disable,
    }
}
