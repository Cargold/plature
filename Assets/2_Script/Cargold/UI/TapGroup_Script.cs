﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;

public class TapGroup_Script : MonoBehaviour
{
    private WhichOne<TapElem_Script> whichOneClass;
    public TapElem_Script[] tapClassArr;
    
	public void Init_Func(int _enableSlotNum, Turret_Attack_Script[] _attackTurretClassArr)
	{
        for (int i = 0; i < tapClassArr.Length; i++)
        {
            if (i < _attackTurretClassArr.Length)
            {
                tapClassArr[i].Init_Func(this, TapElem_Script.TapElemState.SetTurret, _attackTurretClassArr[i]);
            }
            else if (i < _enableSlotNum)
            {
                tapClassArr[i].Init_Func(this, TapElem_Script.TapElemState.WaitTurret, null);
            }
            else
            {
                tapClassArr[i].Init_Func(this, TapElem_Script.TapElemState.Disable, null);
            }
        }

        whichOneClass = new WhichOne<TapElem_Script>();
        whichOneClass.Selected_Func(tapClassArr[0]);
    }
    public void AddTurret_Func(Turret_Attack_Script _attackTurretClass)
    {

    }
    public void ReplaceTurret_Func()
    {

    }
    public void Selected_Func(TapElem_Script _elemClass)
    {
        whichOneClass.Selected_Func(_elemClass);
    }

    public Turret_Attack_Script GetSelectedTurret_Func()
    {
        TapElem_Script _elemClass = this.whichOneClass.GetWhichOne_Func();
        return _elemClass.attackTurretClass;
    }
}
