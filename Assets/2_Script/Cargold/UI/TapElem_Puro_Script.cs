﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TapElem_Puro_Script : MonoBehaviour
{
    public Image iconImg;
    public Text levelTxt;
    public Text nameTxt;
    public Image hpImg;
    public GameObject selectedObj;

	public void Init_Func(Sprite _iconImg, string _level, string _name)
	{
        iconImg.sprite = _iconImg;
        levelTxt.text = string.Format("Lv. {0}", _level);
        nameTxt.text = _name;

        this.SetHp_Func(1f);
        this.Selected_Func(false);
	}
    public void Init_Func()
    {
        iconImg.sprite = null;
        levelTxt.text = "없음";
        nameTxt.text = "없음";

        this.SetHp_Func(0f);
        this.Selected_Func(false);
    }

    public void SetHp_Func(float _value)
    {
        hpImg.fillAmount = 1f;
    }

    public void Selected_Func(bool _isOn)
    {
        selectedObj.SetActive(_isOn);
    }
}
