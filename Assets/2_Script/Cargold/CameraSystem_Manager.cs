﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSystem_Manager : MonoBehaviour
{
    public static CameraSystem_Manager Instance;

    [SerializeField] private Camera thisCamera;
    [SerializeField] private Transform cameraTrf;

    public void Init_Func()
    {
        Instance = this;

        this.EnterLobby_Func();
    }

    public void GameStart_Func()
    {
        cameraTrf.position = new Vector3(0f, 0f, -10f);
        thisCamera.orthographicSize = 15f;
    }

    public void EnterLobby_Func()
    {
        cameraTrf.position = new Vector3(0f, 2.86f, -10f);
        thisCamera.orthographicSize = 1.1f;
    }
}
