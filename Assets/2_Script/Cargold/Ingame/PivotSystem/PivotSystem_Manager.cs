﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.DataStructure;

public class PivotSystem_Manager : MonoBehaviour, IGameState
{
    public static PivotSystem_Manager Instance;
    private Transform platureTrf;
    private Dictionary<PivotSystem_Script, Transform[]> activePivotTrfArrDic;
    private Queue_C<Transform[]> pivotQueue;

    public void Init_Func()
    {
        Instance = this;
        
        // Cargold
        // 플래쳐의 트랜스폼을 중심 피벗으로 사용할 거임.
        // 일단 임시로 자기 트폼을 넣고 나중에 수정할 것
        platureTrf = this.transform;

        activePivotTrfArrDic = new Dictionary<PivotSystem_Script, Transform[]>();
        pivotQueue = new Queue_C<Transform[]>();
    }
    
    public void Move_Func(PivotSystem_Script _pivotClass, DirectionType _dirType, float _velocity, bool _isRotation = true)
    {
        Transform _pivotTrf = null;
        Transform _handleTrf = null;
        this.GetTrfArr_Func(_pivotClass, out _pivotTrf, out _handleTrf);

        switch (_dirType)
        {
            case DirectionType.Up:
                _handleTrf.localPosition += (Vector3.up * _velocity);
                _pivotClass.TargetTrf.position = _handleTrf.position;
                break;
            case DirectionType.Left:
                _pivotTrf.eulerAngles += (Vector3.forward * _velocity);
                _pivotClass.TargetTrf.position = _handleTrf.position;
                _pivotClass.TargetTrf.rotation = _isRotation ? _handleTrf.rotation : _pivotClass.TargetTrf.rotation;
                break;
            case DirectionType.Right:
                _pivotTrf.eulerAngles += (Vector3.back * _velocity);
                _pivotClass.TargetTrf.position = _handleTrf.position;
                _pivotClass.TargetTrf.rotation = _isRotation ? _handleTrf.rotation : _pivotClass.TargetTrf.rotation;
                break;
            case DirectionType.Down:
                _handleTrf.localPosition += (Vector3.down * _velocity);
                _pivotClass.TargetTrf.position = _handleTrf.position;
                break;
            default:
                throw new System.Exception("잘못된 DirectionType이 입력됐습니다. : " + _dirType);
        }
    }
    public void SetPos_Func(PivotSystem_Script _pivotClass, Vector2 _setPos, bool _isRotation = true)
    {
        Transform _pivotTrf = null;
        Transform _handleTrf = null;
        this.GetTrfArr_Func(_pivotClass, out _pivotTrf, out _handleTrf);

        _pivotTrf.LootAt_Func(_setPos);
        _handleTrf.position = _setPos;

        _pivotClass.TargetTrf.position = _handleTrf.position;
        _pivotClass.TargetTrf.rotation = _isRotation ? _handleTrf.rotation : _pivotClass.TargetTrf.rotation;
    }
    public void SetRev_Func(PivotSystem_Script _pivotClass, float _revolveValue, bool _isRotation = true)
    {
        Transform _pivotTrf = null;
        Transform _handleTrf = null;
        this.GetTrfArr_Func(_pivotClass, out _pivotTrf, out _handleTrf);

        _pivotTrf.eulerAngles = Vector3.forward * _revolveValue;

        _pivotClass.TargetTrf.position = _handleTrf.position;
        _pivotClass.TargetTrf.rotation = _isRotation ? _handleTrf.rotation : _pivotClass.TargetTrf.rotation;
    }
    public void ClearPivot_Func(PivotSystem_Script _pivotClass)
    {
        Transform[] _trfArr = this.activePivotTrfArrDic.GetValue_Func(_pivotClass);

        this.activePivotTrfArrDic.Remove_Func(_pivotClass);

        this.pivotQueue.Enqueue_Func(_trfArr);
    }
    private void GetTrfArr_Func(PivotSystem_Script _keyClass, out Transform _pivotTrf, out Transform _handleTrf)
    {
        Transform[] _trfArr = null;
        
        if (this.activePivotTrfArrDic.TryGetValue(_keyClass, out _trfArr) == true)
        {
            _pivotTrf = _trfArr[0];
            _handleTrf = _trfArr[1];
        }
        else
        {
            if (pivotQueue.Dequeue_Func(out _trfArr) == true)
            {

            }
            else
            {
                _trfArr = new Transform[2];

                GameObject _pivotObj_New = new GameObject("Pivot");
                _trfArr[0] = _pivotObj_New.transform;
                _trfArr[0].SetParent(this.platureTrf);

                GameObject _handleObj_New = new GameObject("Handle");
                _trfArr[1] = _handleObj_New.transform;
                _trfArr[1].SetParent(_trfArr[0]);
            }

            _pivotTrf = _trfArr[0];
            _handleTrf = _trfArr[1];

            this.activePivotTrfArrDic.Add_Func(_keyClass, _trfArr);
        }
    }
    public float GetDistanceToPlature_Func(PivotSystem_Script _keyClass)
    {
        if(this.activePivotTrfArrDic.ContainsKey(_keyClass) == true)
        {
            Transform _pivotTrf = null;
            Transform _handleTrf = null;
            this.GetTrfArr_Func(_keyClass, out _pivotTrf, out _handleTrf);

            float _distance = _handleTrf.localPosition.y;

            return _distance;
        }
        else
        {
            return -1f;
        }
    }

    public void StageEnter_Func()
    {

    }
    public void StageFinish_Func()
    {

    }
    public void LobbyEnter_Func()
    {

    }
}
