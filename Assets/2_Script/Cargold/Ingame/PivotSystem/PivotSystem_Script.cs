﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PivotSystem_Script
{
    private Transform targetTrf;
    public Transform TargetTrf { get { return targetTrf; } } 

    // 피벗 시스템에 통제 받을 객체의 트폼 넣으셈
    public PivotSystem_Script(Transform _targetTrf)
	{
        targetTrf = _targetTrf;
    }

    // 이동 방향, 속력 입력
    // _isRotation : 플래쳐를 밑으로 하여 객체의 회전 여부
    public void Move_Func(DirectionType _dirType, float _velocity, bool _isRotation = true)
    {
        PivotSystem_Manager.Instance.Move_Func(this, _dirType, _velocity, _isRotation);
    }

    // 피벗 시스템의 통제를 받으면서 특정 지점으로 Set할 경우 호출
    public void SetPos_Func(Vector2 _setPos, bool _isRotation = true)
    {
        PivotSystem_Manager.Instance.SetPos_Func(this, _setPos, _isRotation);
    }

    // 피벗 시스템의 통제를 받으면서 플래쳐를 공전할 경우 호출
    // Revolution, Revolve = 공전, 공전하다
    public void SetRev_Func(float _revolveValue, bool _isRotation = true)
    {
        PivotSystem_Manager.Instance.SetRev_Func(this, _revolveValue, _isRotation);
    }

    // 피벗 시스템의 통제에서 벗어날 경우(예 = 사망) 호출할 것. (풀링하고 있음)
    public void ClearPivot_Func()
    {
        PivotSystem_Manager.Instance.ClearPivot_Func(this);
    }

    // 플래쳐와의 거리
    public float GetDistanceToPlature_Func()
    {
        return PivotSystem_Manager.Instance.GetDistanceToPlature_Func(this);
    }
}
