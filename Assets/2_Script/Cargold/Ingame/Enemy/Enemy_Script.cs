﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoveState = Enemy_Move_Script.MoveState;
using DG.Tweening;

public class Enemy_Script : MonoBehaviour
{
    public EnemyType enemyType;

    public float healthPoint_Default;
    public float healthPoint;
    protected EnemyState enemyState;
    public EnemyState EnemyState_Get { get { return this.enemyState; } }

    protected PivotSystem_Script pivotClass;
    [SerializeField] protected Enemy_Move_Script moveClass;

    private Transform thisTrf;

    public void Init_Func()
    {
        pivotClass = new PivotSystem_Script(this.transform);

        moveClass.Init_Func(this, this.pivotClass);

        enemyState = EnemyState.None;

        thisTrf = this.transform;

        this.gameObject.SetActive(false);
    }

    public void Generate_Func(Vector2 _setPos)
    {
        this.gameObject.SetActive(true);

        enemyState = EnemyState.Alive;

        healthPoint = healthPoint_Default;

        pivotClass.SetPos_Func(_setPos);

        moveClass.Generate_Func();
    }

    public void Damaged_Func(float _damageValue)
    {
        if(0f < this.healthPoint - _damageValue)
        {
            this.healthPoint -= _damageValue;

            thisTrf.DOScale(1.2f, 0.05f).OnComplete(delegate ()
            {
                thisTrf.DOScale(1f, 0.05f);
            });
        }
        else
        {
            this.healthPoint = 0f;

            this.Die_Func();
        }
    }

    public void Die_Func()
    {
        if(enemyState == EnemyState.Alive)
        {
            enemyState = EnemyState.Die;

            StageSystem_Manager.Instance.Kill_Func();

            this.pivotClass.ClearPivot_Func();

            EnemyPool_Manager.Instance.EnemyReturn_Func(this);

            int _turretPer = Random.Range(0, 10);
            if(0 < _turretPer)
            {

            }
            else
            {
                TurretSystem_Manager.instance.AddTurret(TurretType.Rifle);
            }

            this.gameObject.SetActive(false);
        }
        else
        {

        }
    }

    public enum EnemyState
    {
        None = -1,
        Alive,
        Die,
    }
}
