﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.DataStructure;

public class EnemyPool_Manager : MonoBehaviour, IGameState
{
    public static EnemyPool_Manager Instance;

    private Dictionary<EnemyType, Queue_C<Enemy_Script>> enemyPoolDic;

    public GameObject enemyObj_Normal;
    public GameObject enemyObj_Speed;
    public GameObject enemyObj_Tank;
    public GameObject enemyObj_Range;
    private Dictionary<EnemyType, GameObject> enemyObjDIc;

    public void Init_Func()
    {
        Instance = this;

        // Cargold : 나머지 타입도 추가하자
        enemyObjDIc = new Dictionary<EnemyType, GameObject>();
        enemyObjDIc.Add_Func(EnemyType.Speed, enemyObj_Speed);
        enemyObjDIc.Add_Func(EnemyType.Tank, enemyObj_Tank);
        enemyObjDIc.Add_Func(EnemyType.Range, enemyObj_Range);

        enemyPoolDic = new Dictionary<EnemyType, Queue_C<Enemy_Script>>();
        this.Init_GenerateEnemy_Func(EnemyType.Speed);
        this.Init_GenerateEnemy_Func(EnemyType.Tank);
        this.Init_GenerateEnemy_Func(EnemyType.Range);
    }

    void Init_GenerateEnemy_Func(EnemyType _enemyType)
    {
        Queue_C<Enemy_Script> _enemyPool = new Queue_C<Enemy_Script>();

        for (int i = 0; i < 10; i++)
        {
            Enemy_Script _enemyClass = this.GenerateEnemyClass_Func(_enemyType);

            _enemyPool.Enqueue_Func(_enemyClass);
        }

        enemyPoolDic.Add_Func(_enemyType, _enemyPool);
    }

    private Enemy_Script GenerateEnemyClass_Func(EnemyType _enemyType)
    {
        GameObject _enemyObj_selected = this.enemyObjDIc.GetValue_Func(_enemyType);

        GameObject _enemyObj = GameObject.Instantiate(_enemyObj_selected);
        Enemy_Script _enemyClass = _enemyObj.GetComponent<Enemy_Script>();
        _enemyClass.Init_Func();

        return _enemyClass;
    }

    public Enemy_Script GetEnemy_Func(EnemyType _enemyType)
    {
        var _enemyPool = this.enemyPoolDic.GetValue_Func(_enemyType);

        Enemy_Script _enemyClass = null;
        if(_enemyPool.Dequeue_Func(out _enemyClass) == true) { }
        else
        {
            _enemyClass = this.GenerateEnemyClass_Func(_enemyType);
        }

        return _enemyClass;
    }

    public void EnemyReturn_Func(Enemy_Script _enemyClass)
    {
        EnemyType _enemyType = _enemyClass.enemyType;

        var _enemyPool = this.enemyPoolDic.GetValue_Func(_enemyType);

        _enemyPool.Enqueue_Func(_enemyClass);
    }

    public void LobbyEnter_Func()
    {

    }

    public void StageEnter_Func()
    {

    }

    public void StageFinish_Func()
    {

    }
}
