﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnemyState = Enemy_Script.EnemyState;

public class Enemy_Move_Script : MonoBehaviour
{
    private Enemy_Script enemyClass;
    private PivotSystem_Script pivotClass;
    private MoveState moveState;
    public MoveState MoveState_Get { get { return moveState; } }
    public float orbitHeight;
    public float MoveSpeed_Default;
    private float moveSpeed;
    private float orbitSpeed;
    public float OrbitWaitTime;

    public void Init_Func(Enemy_Script _enemyClass, PivotSystem_Script _pivotClass)
	{
        enemyClass = _enemyClass;
        pivotClass = _pivotClass;

        moveState = MoveState.None;

        moveSpeed = MoveSpeed_Default * 0.01f;
        orbitSpeed = MoveSpeed_Default * 0.1f;
    }

    public void Generate_Func()
    {
        StartCoroutine(Move_Cor());

        SetState_Func(MoveState.ApproachOrbit);
    }

    public void SetState_Func(MoveState _moveState)
    {
        if(_moveState == MoveState.Revolution)
        {
            if(this.moveState != MoveState.Revolution)
            {
                this.moveState = MoveState.Revolution;

                StartCoroutine(WaitOrbit_Cor());
            }
            else
            {
                throw new System.Exception("에너미의 공전 상태가 중복 호출됨");
            }
        }
        else
        {
            this.moveState = _moveState;
        }
    }

    private IEnumerator Move_Cor()
    {
        while (enemyClass.EnemyState_Get == EnemyState.Alive)
        {
            switch (this.moveState)
            {
                case MoveState.None:
                    break;
                case MoveState.ApproachOrbit:
                    float _dist1 = pivotClass.GetDistanceToPlature_Func();
                    if(orbitHeight < _dist1)
                    {
                        pivotClass.Move_Func(DirectionType.Down, moveSpeed);
                    }
                    else
                    {
                        this.SetState_Func(MoveState.Revolution);
                    }

                    break;
                case MoveState.Revolution:
                    pivotClass.Move_Func(DirectionType.Right, orbitSpeed);
                    break;
                case MoveState.ContactPlature:
                    float _dist2 = pivotClass.GetDistanceToPlature_Func();
                    if (0f < _dist2)
                    {
                        pivotClass.Move_Func(DirectionType.Down, moveSpeed);
                    }
                    else
                    {
                        throw new System.Exception("플래쳐와의 거리가 0입니다.");
                    }
                    break;
            }

            yield return Cargold_Library.GetWaitForSeconds_Func(0.02f);
        }
    }

    private IEnumerator WaitOrbit_Cor()
    {
        yield return Cargold_Library.GetWaitForSeconds_Func(OrbitWaitTime);

        this.SetState_Func(MoveState.ContactPlature);
    }

    public enum MoveState
    {
        None = -1,
        ApproachOrbit,
        Revolution,
        ContactPlature,
    }
}
