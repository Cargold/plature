﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Time_Script : MonoBehaviour, IGameState
{
    public Text timeTxt;

    public void Init_Func()
    {
        timeTxt.text = "";
    }

    public void StageEnter_Func()
    {
        //StartCoroutine(OnTimer_Cor());
    }

    IEnumerator OnTimer_Cor()
    {
        WaitForFixedUpdate _delay = new WaitForFixedUpdate();
        float _time = 0f;

        while (true)
        {
            _time += 0.02f;

            timeTxt.text = ((int)_time).ToString();

            yield return _delay;
        }
    }

    public void StageFinish_Func()
    {

    }

    public void LobbyEnter_Func()
    {

    }
}
