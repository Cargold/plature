﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageUI_Manager : MonoBehaviour, IGameState
{
    public static StageUI_Manager Instance;

    public UI_Time_Script timeClass;
    [SerializeField] private JoyStickController_Script[] joyStickClassArr;
    
    public List<IGameState> iGameStateList;

    public void Init_Func()
    {
        timeClass.Init_Func();

        for (int i = 0; i < joyStickClassArr.Length; i++)
        {
            joyStickClassArr[i].Init_Func();
        }

        iGameStateList = new List<IGameState>();
        iGameStateList.Add(this.timeClass);
    }

    public void LobbyEnter_Func()
    {
        foreach (var item in iGameStateList)
        {
            item.LobbyEnter_Func();
        }
    }

    public void StageEnter_Func()
    {
        foreach (var item in iGameStateList)
        {
            item.StageEnter_Func();
        }
    }

    public void StageFinish_Func()
    {
        foreach (var item in iGameStateList)
        {
            item.StageFinish_Func();
        }
    }
}
