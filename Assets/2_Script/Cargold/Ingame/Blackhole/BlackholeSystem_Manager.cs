﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.DataStructure;

public class BlackholeSystem_Manager : MonoBehaviour, IGameState
{
    public static BlackholeSystem_Manager Instance;
    public GameObject enemyObj;

    public GameObject blackholeObj;
    public float generateDistance;
    public float bHoleGenInterval;
    public int level;

    public void Init_Func()
    {
        Instance = this;
    }

    public void StageEnter_Func()
    {
        StartCoroutine(Level_Cor());
        StartCoroutine(Generate_Cor());
    }

    private IEnumerator Level_Cor()
    {
        while(true)
        {
            yield return Cargold_Library.GetWaitForSeconds_Func(10f);

            level += 1;
        }
    }
    private IEnumerator Generate_Cor()
    {
        WaitForSeconds _delay = new WaitForSeconds(bHoleGenInterval);

        while(true)
        {
            GameObject _blackholeObj = GameObject.Instantiate(this.blackholeObj);

            Blackhole_Script _bHoleClass = _blackholeObj.GetComponent<Blackhole_Script>();
            _bHoleClass.Init_Func();

            Vector2 _platurePos = Vector2.zero; // Cargold : 플래쳐 위치값
            float _randAngle = Random.Range(0f, 360f);
            Vector2 _bHolePos = Cargold_Library.GetCircumferencePos_Func(_platurePos, generateDistance, _randAngle);

            Enemy_Script[] _spawnEnemyClassArr = GetEnemyClassArr_Func(level+2);
            _bHoleClass.Generate_Func(_bHolePos, _spawnEnemyClassArr);

            yield return _delay;
        }
    }
    private Enemy_Script[] GetEnemyClassArr_Func(int _num)
    {
        List<Enemy_Script> _enemyList = new List<Enemy_Script>();

        Enemy_Script _enemyClass = null;

        for (int i = 0; i < _num; i++)
        {
            EnemyType _enemyType = (EnemyType)Random.Range(1, 4);

            _enemyClass = EnemyPool_Manager.Instance.GetEnemy_Func(_enemyType);
            _enemyList.AddNewItem_Func(_enemyClass);
        }

        return _enemyList.ToArray();
    }

    public void LobbyEnter_Func()
    {

    }

    public void StageFinish_Func()
    {

    }
}
