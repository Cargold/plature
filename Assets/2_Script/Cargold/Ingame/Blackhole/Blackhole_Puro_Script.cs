﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Blackhole_Puro_Script : MonoBehaviour
{
    public Blackhole_Script mainClass;
    public Transform bodyTrf;
    public float rotateSpeed;

    public void Init_Func()
    {
        
    }

    // 블랙홀 생성 연출
    public IEnumerator Activation_Cor()
    {
        bodyTrf.localScale = Vector3.one * 0.01f;
        bodyTrf.DOScale(1f, 1f);

        StartCoroutine(Rotate_Cor());

        yield return Cargold_Library.GetWaitForSeconds_Func(1f);

        yield break;
    }

    private IEnumerator Rotate_Cor()
    {
        while(mainClass.isActiveState == true)
        {
            bodyTrf.Rotate(Vector3.forward * rotateSpeed * 0.01f);

            yield return Cargold_Library.GetWaitForSeconds_Func(0.01f);
        }
    }

    // 블랙홀 파괴 연출
    public IEnumerator Deactivation_Cor()
    {
        bodyTrf.DOScale(0.01f, 1f);

        yield return Cargold_Library.GetWaitForSeconds_Func(1f);
        
        yield break;
    }
}
