﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blackhole_Script : MonoBehaviour
{
    [SerializeField] private Blackhole_Puro_Script puroClass;

    private Enemy_Script[] enemyClassArr;
    public float spawnInterval;
    public bool isActiveState;

	public void Init_Func()
	{
        puroClass.Init_Func();

        isActiveState = false;
    }

    public void Generate_Func(Vector2 _pos, Enemy_Script[] _enemyClassArr)
    {
        isActiveState = true;

        this.enemyClassArr = _enemyClassArr;

        this.transform.position = _pos;

        StartCoroutine(Generate_Cor(_pos));
    }
    IEnumerator Generate_Cor(Vector2 _pos)
    {
        yield return puroClass.Activation_Cor();

        yield return SpawnEnemy_Cor(_pos);
    }
    
    IEnumerator SpawnEnemy_Cor(Vector2 _pos)
    {
        for (int i = 0; i < this.enemyClassArr.Length; i++)
        {
            // 몬스터 생성

            Enemy_Script _enemyClass = this.enemyClassArr[i];

            _enemyClass.Generate_Func(_pos);

            yield return Cargold_Library.GetWaitForSeconds_Func(spawnInterval);
        }

        this.Deactivation_Func();
    }

    public void Die_Func()
    {
        
    }

    public void Deactivation_Func()
    {
        StartCoroutine(Deactivation_Cor());
    }
    private IEnumerator Deactivation_Cor()
    {
        yield return puroClass.Deactivation_Cor();

        this.isActiveState = false;

        GameObject.Destroy(this.gameObject);
    }
}
