﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.ResourceFindPath;

public class ResourceLoad_Manager : ResourceFindPath<ResourceLoadType>
{
    public static ResourceLoad_Manager Instance;
     
	public override void Init_Func()
	{
        base.Init_Func();

        Instance = this;

        base.SetPath_Func(ResourceLoadType.DatabaseSystem, "Cargold/ResourceLoad/Prefab/DataBase_Manager");
        base.SetPath_Func(ResourceLoadType.PlayerSystem, "Cargold/ResourceLoad/Prefab/PlayerSystem_Manager");
        base.SetPath_Func(ResourceLoadType.LobbySytem, "Cargold/ResourceLoad/Prefab/LobbySystem_Manager");
        base.SetPath_Func(ResourceLoadType.CameraSystem, "Cargold/ResourceLoad/Prefab/CameraSystem_Manager");

        base.SetPath_Func(ResourceLoadType.StageSystemGroup, "Cargold/ResourceLoad/Prefab/StageSystem_Group");
    }
}