﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSystem_Manager : MonoBehaviour, IGameState
{
    public static StageSystem_Manager Instance;
    [SerializeField] private StageSystem_Puro_Manager puroClass;
    
    public Plature_Manager platureManager;

    public List<IGameState> iGameStateList;

    public int stage;
    public int score;
    public int gold;

    public void Init_Func()
    {
        Instance = this;

        BlackholeSystem_Manager _bHoleManager = FindObjectOfType<BlackholeSystem_Manager>();
        _bHoleManager.Init_Func();

        EnemyPool_Manager _enemyPoolManager = FindObjectOfType<EnemyPool_Manager>();
        _enemyPoolManager.Init_Func();

        PivotSystem_Manager _pivotManager = FindObjectOfType<PivotSystem_Manager>();
        _pivotManager.Init_Func();
        StageUI_Manager _stageUiManager = FindObjectOfType<StageUI_Manager>();
        _stageUiManager.Init_Func();

        Plature_Manager _platureManager = FindObjectOfType<Plature_Manager>();
        _platureManager.Init_Func();

        iGameStateList = new List<IGameState>();
        iGameStateList.Add(_bHoleManager);
        iGameStateList.Add(_enemyPoolManager);
        iGameStateList.Add(_pivotManager);
        iGameStateList.Add(_stageUiManager);
        iGameStateList.Add(platureManager);

        stage = 0;
    }

    public void StageEnter_Func()
    {
        for (int i = 0; i < iGameStateList.Count; i++)
        {
            iGameStateList[i].StageEnter_Func();
        }

        TurretSystem_Manager.instance.AddTurret(TurretType.Rifle);
    }

    public void Kill_Func()
    {
        this.score += 123;
        this.gold += 7;
    }

    public void StageFinish_Func()
    {

    }

    public void LobbyEnter_Func()
    {

    }
}
