﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBase_Manager : MonoBehaviour, ReturnTurretDB
{
    public static DataBase_Manager Instance;

    [SerializeField] private Dictionary<string, Plature_Data> platureDataDic;
    [SerializeField] private Plature_Data_Prefab[] platureDataPrefabArr;
    public int PlatureDataNum { get { return platureDataPrefabArr.Length; } }

    [SerializeField] private Dictionary<TurretType, Turret_Data> turretDataDic;
    [SerializeField] private Turret_Data_Prefab[] turretDataPrefabArr;
    public int TurretDataNum { get { return turretDataPrefabArr.Length; } }

    public void Init_Func()
    {
        Instance = this;
        Init_PlatureData_Func();
        Init_TurretData_Func();
    }

    private void Init_PlatureData_Func()
    {
        platureDataDic = new Dictionary<string, Plature_Data>();
        for (int i = 0; i < platureDataPrefabArr.Length; i++)
        {
            Plature_Data_Prefab _dataPrefab = platureDataPrefabArr[i];

            Plature_Data _platureData = new Plature_Data();
            _platureData.platureID = _dataPrefab.platureData.platureID;
            _platureData.bodySprite = _dataPrefab.platureData.bodySprite;
            _platureData.topSprite = _dataPrefab.platureData.topSprite;
            _platureData.hp_max = _dataPrefab.platureData.hp_max;
            _platureData.hp_increase = _dataPrefab.platureData.hp_increase;

            this.platureDataDic.Add(_dataPrefab.platureData.platureID, _platureData);
        }
    }

    private void Init_TurretData_Func()
    {
        turretDataDic = new Dictionary<TurretType, Turret_Data>();
        for (int i = 0; i < turretDataPrefabArr.Length; i++)
        {
            Turret_Data_Prefab _dataPrefab = turretDataPrefabArr[i];

            this.turretDataDic.Add(_dataPrefab.turretData.id, _dataPrefab.turretData);
        }
    }

    public Plature_Data GetPlatureData_Func(string _id)
    {
        return this.platureDataDic.GetValue_Func(_id);
    }
    public Plature_Data[] GetPlatureData_Func()
    {
        return this.platureDataDic.GetValue_Func();
    }

    public Turret_Data ReturnTurretData(TurretType turretDB)
    {
        return this.turretDataDic.GetValue_Func(turretDB);
    }
}
