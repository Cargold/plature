﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gallery_Script : MonoBehaviour, ILobbyMenu
{
    public Gallery_Puro_Script puroClass;

    public void Init_Func()
    {
        puroClass.Init_Func();
    }
    public void Enter_Func()
    {
        puroClass.Enter_Func(true);
    }
    public void Exit_Func()
    {
        puroClass.Enter_Func(false);
    }
}
