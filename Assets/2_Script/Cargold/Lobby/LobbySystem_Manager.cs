﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbySystem_Manager : MonoBehaviour
{
    public static LobbySystem_Manager Instance;

    public MainLobby_Script mainClass;
    public Upgrade_Script upgradeClass;
    public Gallery_Script galleryClass;
    public Shop_Script shopClass;
    private Dictionary<LobbyState, ILobbyMenu> lobbyMenuDic;

    public LobbyState lobbyState;

    public void Init_Func()
	{
        Instance = this;

        lobbyMenuDic = new Dictionary<LobbyState, ILobbyMenu>();
        lobbyMenuDic.Add(LobbyState.Main, mainClass);
        lobbyMenuDic.Add(LobbyState.Upgrade, upgradeClass);
        lobbyMenuDic.Add(LobbyState.Gallery, galleryClass);
        lobbyMenuDic.Add(LobbyState.Shop, shopClass);
        
        mainClass.Init_Func();
        upgradeClass.Init_Func();
        galleryClass.Init_Func();
        shopClass.Init_Func();

        lobbyState = LobbyState.None;
    }
    public void Enter_Func(LobbyState _lobbyState)
    {
        if(lobbyState == LobbyState.Main && _lobbyState == LobbyState.Main)
        {
            this.GameStart_Func();
        }
        else
        {
            lobbyState = _lobbyState;

            ILobbyMenu _lobbyMenu = lobbyMenuDic.GetValue_Func(_lobbyState);
            _lobbyMenu.Enter_Func();
        }
    }
    private void GameStart_Func()
    {
        GameSystem_Manager.Instance.GameStart_Func();
    }

    private void Back_Func()
    {
        ILobbyMenu _lobbyMenu = lobbyMenuDic.GetValue_Func(lobbyState);
        _lobbyMenu.Exit_Func();

        _lobbyMenu = lobbyMenuDic.GetValue_Func(LobbyState.Main);
        _lobbyMenu.Enter_Func();
    }
    public void CallBtn_Func(string _key)
    {
        switch (_key)
        {
            case "Back":
                Back_Func();
                break;
        }
    }
}

public enum LobbyState
{
    None = -1,
    Main,
    Upgrade,
    Shop,
    Max,

    Gallery,
}

public interface ILobbyMenu
{
    void Enter_Func();
    void Exit_Func();
}