﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop_Script : MonoBehaviour, ILobbyMenu
{
    public Shop_Puro_Script puroClass;

    public void Init_Func()
    {
        puroClass.Init_Func();
    }
    public void Enter_Func()
    {
        puroClass.Enter_Func(true);
    }
    public void Exit_Func()
    {
        puroClass.Enter_Func(false);
    }
}
