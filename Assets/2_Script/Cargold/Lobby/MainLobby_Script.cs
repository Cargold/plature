﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.DataStructure;

public class MainLobby_Script : MonoBehaviour, ILobbyMenu
{
    public MainLobby_Puro_Script puroClass;
    
    private CirculateQueue<LobbyState> lobbyStateCQ;

	public void Init_Func()
	{
        puroClass.Init_Func();

        lobbyStateCQ = new CirculateQueue<LobbyState>();
        lobbyStateCQ.Enqueue_Func(LobbyState.Main, LobbyState.Upgrade, LobbyState.Shop);

        this.SetMenuTitle_Func();
    }
    private void SetMenuTitle_Func()
    {
        string _name = "";

        switch (lobbyStateCQ.GetItem)
        {
            case LobbyState.Main:
                _name = "게임 시작";
                break;
            case LobbyState.Upgrade:
                _name = "플래쳐";
                break;
            case LobbyState.Gallery:
                _name = "갤러리";
                break;
            case LobbyState.Shop:
                _name = "상점";
                break;
        }

        puroClass.SetTitle_Func(_name);
    }
    private void SelectMenu_Func()
    {
        this.Exit_Func();

        LobbySystem_Manager.Instance.Enter_Func(lobbyStateCQ.GetItem);
    }
    
    public void Enter_Func()
    {
        puroClass.SetObj_Func(true);

        this.SetMenuTitle_Func();
    }

    public void Exit_Func()
    {
        puroClass.SetObj_Func(false);
    }

    public void CallBtn_Func(string _key)
    {
        switch (_key)
        {
            case "Left":
                lobbyStateCQ.Dequeue_Func(true);
                SetMenuTitle_Func();
                break;
            case "Right":
                lobbyStateCQ.Dequeue_Func();
                SetMenuTitle_Func();
                break;
            case "Enter":
                this.SelectMenu_Func();
                break;
        }
    }
}
