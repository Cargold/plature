﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade_Script : MonoBehaviour, ILobbyMenu
{
    public Upgrade_Puro_Script puroClass;

    public UpgradeInfo_Script infoClass;

    public void Init_Func()
	{
        puroClass.Init_Func();

        infoClass.Init_Func();
    }
    public void Enter_Func()
    {
        Plature_Script _platureClass = PlayerSystem_Manager.Instance.GetSelectedPlature_Func();
        infoClass.SetPlature_Func(_platureClass);

        puroClass.Enter_Func(true);
    }
    public void Exit_Func()
    {
        puroClass.Enter_Func(false);
    }
    public void CallBtn_Func(string _key)
    {
        switch (_key)
        {
            case "Select":
                break;
            case "Back":
                break;
            case "Left":
                break;
            case "Right":
                break;
        }
    }
}
