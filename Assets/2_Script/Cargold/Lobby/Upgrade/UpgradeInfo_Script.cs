﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeInfo_Script : MonoBehaviour
{
    public UpgradeInfo_Puro_Script puroClass;

	public void Init_Func()
	{
        puroClass.Init_Func();
    }

    public void SetPlature_Func(Plature_Script _platureClass)
    {
        puroClass.SetPlature_Func(_platureClass.platureName, _platureClass.level, _platureClass.hp_max);
    }
}
