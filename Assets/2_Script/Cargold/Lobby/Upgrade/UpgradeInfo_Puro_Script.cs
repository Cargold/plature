﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeInfo_Puro_Script : MonoBehaviour
{
    public Text nameTxt;
    public Text levelTxt;
    public Text hpTxt;

    public void Init_Func()
	{

	}

    public void SetPlature_Func(string _name, int _level, int _hp)
    {
        nameTxt.text = _name;
        this.SetStat_Func(_level, _hp);
    }

    public void SetStat_Func(int _level, int _hp)
    {
        levelTxt.text = _level.ToString();
        hpTxt.text = _hp.ToString();
    }
}
