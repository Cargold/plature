﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainLobby_Puro_Script : MonoBehaviour
{
    public Text titleTxt;
    public GameObject[] objArr;

	public void Init_Func()
	{
        this.gameObject.SetActive(false);
	}

    public void SetTitle_Func(string _name)
    {
        titleTxt.text = _name;
    }

    public void SetObj_Func(bool _isOn)
    {
        foreach (var item in objArr)
        {
            item.SetActive(_isOn);
        }
    }
}
