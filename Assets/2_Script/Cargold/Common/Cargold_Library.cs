﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Cargold_Library
{
    #region Array Group
    public static bool Contatin_Func(this string[] _arr, string _value, CL_ArrayContainSearchType _type = CL_ArrayContainSearchType.StartIndexToBegin)
    {
        // 배열에 특정 값이 있는지 확인

        if (_type == CL_ArrayContainSearchType.StartIndexToBegin)
        {
            for (int i = 0; i < _arr.Length; i++)
            {
                if (_arr[i] == _value)
                {
                    return true;
                }
            }
        }
        else if (_type == CL_ArrayContainSearchType.StartIndexToEnd)
        {
            for (int i = _arr.Length - 1; 0 <= i; i--)
            {
                if (_arr[i] == _value)
                {
                    return true;
                }
            }
        }
        else
        {

        }

        return false;
    }
    public enum CL_ArrayContainSearchType
    {
        StartIndexToBegin,
        StartIndexToEnd,
    }
    public static T GetRandItem_Func<T>(this T[] _arr)
    {
        if (_arr == null)
        {
            throw new Exception("배열이 비어있습니다.");
        }
        else
        {
            int _length = _arr.Length;

            int _randValue = UnityEngine.Random.Range(0, _length);

            T _randItem = _arr[_randValue];

            return _randItem;
        }
    }
    #endregion
    #region List Group
    public static bool AddNewItem_Func<ValueType>(this List<ValueType> _list, ValueType _addItem)
    {
        bool _isContain = _list.Contains(_addItem);

        if (_isContain == false)
        {

        }
        else
        {
            Debug.LogWarning("이미 삽입되어 있는 Item을 중복해서 삽입하였습니다. : " + _addItem);
        }

        _list.Add(_addItem);

        return _isContain;
    }
    public static bool Insert_Func<ValueType>(this List<ValueType> _list, int _id, ValueType _addItem)
    {
        bool _isContain = _list.Contains(_addItem);

        if (_isContain == false)
        {

        }
        else
        {
            Debug.LogWarning("이미 삽입되어 있는 Item을 중복해서 삽입하였습니다. : " + _addItem);
        }

        _list.Insert(_id, _addItem);

        return _isContain;
    }
    public static void Remove_Func<ValueType>(this List<ValueType> _list, ValueType _removeItem)
    {
        if (_list.Contains(_removeItem) == true)
        {
            _list.Remove(_removeItem);
        }
        else
        {
            throw new Exception("존재하지 않는 Item을 Remove하고자 합니다. : " + _removeItem);
        }
    }
    public static ValueType GetLastItem_Func<ValueType>(this List<ValueType> _list)
    {
        // 리스트의 마지막 아이템 반환

        int _count = _list.Count;
        return _list[_count - 1];
    }
    public static ValueType GetHalfItem_Func<ValueType>(this List<ValueType> _list)
    {
        // 리스트의 중간에 배치된 아이템 반환

        int _listNum = _list.Count;

        int _halfID = _listNum / 2;

        ValueType _halfItem = _list[_halfID];

        return _halfItem;
    }
    public static ValueType GetRandItem_Func<ValueType>(this List<ValueType> _list)
    {
        int _cnt = _list.Count;
        int _randValue = UnityEngine.Random.Range(0, _cnt);
        return _list[_randValue];
    }
    #endregion
    #region Animation Group
    public static void Play_Func(this Animation _anim, AnimationClip _clip, bool _isRewind = false, bool _isImmediatly = false, float _speed = 1f)
    {
        string _clipName = _clip.name;

        _anim.Play_Func(_clipName, _isRewind, _isImmediatly, _speed);
    }
    public static void Play_Func(this Animation _anim, string _clipName, bool _isRewind = false, bool _isImmediatly = false, float _speed = 1f)
    {
        // _isImmediatly를 사용할 경우 애니메이션 이벤트 함수는 작동 안 됨

        if (_anim != null)
        {
            AnimationClip _clip = _anim.GetClip(_clipName);
            if (_clip != null)
            {
                float _time = 0f;

                if (_isImmediatly == false)
                {
                    if (_isRewind == false)
                    {
                        _time = 0f;
                    }
                    else
                    {
                        _speed *= -1f;

                        _time = _anim[_clipName].length;
                    }
                }
                else
                {
                    _speed = 0f;

                    if (_isRewind == false)
                    {
                        _time = _anim[_clipName].length;
                    }
                    else
                    {
                        _time = 0f;
                    }
                }

                _anim[_clipName].speed = _speed;
                _anim[_clipName].time = _time;
                _anim.Play(_clipName);
            }
            else
            {
                throw new Exception("애니메이션 클립이 없습니다. : " + _clipName);
            }
        }
        else
        {
            throw new Exception("애니메이션 컴포넌트가 비어있습니다. : " + _anim.gameObject.name);
        }
    }
    #endregion
    #region Dictionary Group
    public static void Add_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, KeyType _addKey, ValueType _addValue)
    {
        // 오류 검출용

        if (_dic.ContainsKey(_addKey) == false)
        {
            _dic.Add(_addKey, _addValue);
        }
        else
        {
            throw new Exception("Dictionary에 다음 Key가 이미 존재합니다. : " + _addKey);
        }
    }
    public static void Remove_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, KeyType _removeKey)
    {
        // 오류 검출용

        if (_dic.ContainsKey(_removeKey) == true)
        {
            _dic.Remove(_removeKey);
        }
        else
        {
            throw new Exception("Dictionary에 지우려고 하는 다음 Key가 존재하지 않습니다. : " + _removeKey);
        }
    }

    public static void SetClearToValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _clearDic, ValueType _clearValue)
    {
        // 함수의 Value 인자로 모두 채우기

        int _keyNum = _clearDic.Keys.Count;
        KeyType[] _keyTypeArr = new KeyType[_keyNum];
        _clearDic.Keys.CopyTo(_keyTypeArr, 0);

        for (int i = 0; i < _keyNum; i++)
        {
            KeyType _keyType = _keyTypeArr[i];

            _clearDic[_keyType] = _clearValue;
        }
    }
    public static void SetClearToValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType[]> _clearDic, ValueType _clearValue)
    {
        // 함수의 Value 인자로 모두 채우기

        int _keyNum = _clearDic.Keys.Count;
        KeyType[] _keyTypeArr = new KeyType[_keyNum];
        _clearDic.Keys.CopyTo(_keyTypeArr, 0);

        for (int i = 0; i < _keyNum; i++)
        {
            KeyType _keyType = _keyTypeArr[i];

            int _valueNum = _clearDic[_keyType].Length;
            for (int j = 0; j < _valueNum; j++)
            {
                _clearDic[_keyType][j] = _clearValue;
            }
        }
    }

    public static ValueType GetValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, KeyType _key)
    {
        // Key에 해당하는 Value 반환
        // 오류 검출용

        ValueType _returnValue;
        if (_dic.TryGetValue(_key, out _returnValue) == true)
        {
            return _returnValue;
        }
        else
        {
            throw new System.Exception("Key 없음 : " + _key);
        }
    }
    public static ValueType[] GetValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, params KeyType[] _keyArr)
    {
        // Key에 해당하는 Value 반환
        // 오류 검출용
        // 인자 Key 중 중복Key가 있는지 검사하는 기능도 추가하자

        List<ValueType> _list = new List<ValueType>();

        for (int i = 0; i < _keyArr.Length; i++)
        {
            ValueType _value = _dic.GetValue_Func(_keyArr[i]);
            _list.Add(_value);
        }

        return _list.ToArray();
    }
    public static ValueType[] GetValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic)
    {
        // 딕셔너리의 모든 Value를 배열로 반환

        ValueType[] _returnValueArr = new ValueType[_dic.Values.Count];

        _dic.Values.CopyTo(_returnValueArr, 0);

        return _returnValueArr;
    }
    public static ValueType GetValueRandom_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic)
    {
        // 최적화 필요하므로 자주 사용하지 말 것

        ValueType[] _valueAll = Cargold_Library.GetValue_Func<KeyType, ValueType>(_dic);

        int _randValue = UnityEngine.Random.Range(0, _valueAll.Length);

        return _valueAll[_randValue];
    }
    public static KeyType[] GetKeys_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic)
    {
        // 딕셔너리에 입력된 모든 Key를 반환한다.

        KeyType[] _keyTypeArr = new KeyType[_dic.Keys.Count];
        _dic.Keys.CopyTo(_keyTypeArr, 0);
        return _keyTypeArr;
    }
    public static ValueType[] GetValueRandom_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, int _returnNum)
    {
        // 최적화 필요하므로 자주 사용하지 말 것
        // 중복 Value가 Return될 수도 있음

        ValueType[] _valueAll = Cargold_Library.GetValue_Func<KeyType, ValueType>(_dic);
        List<ValueType> _valueList = new List<ValueType>();
        for (int i = 0; i < _returnNum; i++)
        {
            int _randValue = UnityEngine.Random.Range(0, _valueAll.Length);
            _valueList.Add(_valueAll[_randValue]);
        }

        return _valueList.ToArray();
    }

    public static ValueType ReplaceValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, KeyType _key, ValueType _value)
    {
        // 신규 Value를 삽입하고 기존 Value는 Dictionary에서 제거 후 반환한다.

        ValueType _originalValue = _dic.GetValue_Func(_key);
        _dic.Remove(_key);
        _dic.Add(_key, _value);
        return _originalValue;
    }
    #endregion
    #region Casting Group
    public static int ToInt(this System.Enum value)
    {
        // 이거 GC 발생한다고 함

        var _returnValue = Convert.ChangeType(value, typeof(int));
        return (int)_returnValue;
    }
    public static int ToInt(this float value)
    {
        return (int)value;
    }

    public static T ToEnum<T>(this string value)
    {
        return (T)System.Enum.Parse(typeof(T), value, true);
    }
    public static int ToInt(this string value)
    {
        if (value == "") return 0;

        return System.Int32.Parse(value);
    }
    public static float ToFloat(this string value)
    {
        if (value == "") return 0f;

        float returnValue = 0f;

        System.Single.TryParse(value, out returnValue);

        return returnValue;
    }
    public static bool ToBool(this string value)
    {
        switch (value)
        {
            case "TRUE":
            case "True":
            case "T":
            case "1":
                return true;

            default:
                return false;
        }
    }

    public static Byte ToByte(this string value)
    {
        if (value == "") return 0;

        Byte returnValue = 0;

        System.Byte.TryParse(value, out returnValue);

        return returnValue;
    }

    public static string ToString_Func(this float _value, int _pointNumber = 0)
    {
        if (0 < _pointNumber)
        {
            if (_pointNumber == 1)
            {
                return string.Format("{0:N1}", _value);
            }
            else if (_pointNumber == 2)
            {
                return string.Format("{0:N2}", _value);
            }
            else if (_pointNumber == 3)
            {
                return string.Format("{0:N3}", _value);
            }
            else
            {
                // 부동소수점의 오차범위
                // 4자리수 넘어서까지 쓸 일 있으면 추가 바람

                return string.Format("{0:N4}", _value);
            }
        }
        else
        {
            return string.Format("{0:N0}", _value);
        }
    }

    #endregion
    #region UnityUi Group
    public static void SetFade_Func(this SpriteRenderer _spriteRend, float _alphaValue)
    {
        Color _returnColor = _spriteRend.color;

        _spriteRend.color = GetNaturalAlphaColor_Func(_returnColor, _alphaValue);
    }
    public static void SetFade_Func(this Image _image, float _alphaValue)
    {
        Color _returnColor = _image.color;

        _image.color = GetNaturalAlphaColor_Func(_returnColor, _alphaValue);
    }
    public static void SetFade_Func(this Text _text, float _alphaValue)
    {
        Color _returnColor = _text.color;

        _text.color = GetNaturalAlphaColor_Func(_returnColor, _alphaValue);
    }
    public static void SetFade_Func(this Graphic _graphic, float _alphaValue)
    {
        Color _returnColor = _graphic.color;

        _graphic.color = GetNaturalAlphaColor_Func(_returnColor, _alphaValue);
    }

    public static Color GetNaturalAlphaColor_Func(this Color _color, float _alphaValue)
    {
        Color _returnColor = new Color
            (
            _color.r,
            _color.g,
            _color.b,
            _alphaValue
            );

        return _returnColor;
    }
    public static void SetAlphaOnBaseColor_Func(this Color _color, float _alphaValue)
    {
        Color _setColor = new Color
            (
            _color.r,
            _color.g,
            _color.b,
            _alphaValue
            );

        _color = _setColor;
    }
    public static void SetColorOnBaseAlpha_Func(this Image _image, Color _setColor)
    {
        _setColor = new Color
            (
            _setColor.r,
            _setColor.g,
            _setColor.b,
            _image.color.a
            );

        _image.color = _setColor;
    }
    public static void SetNativeSize_Func(this Image _image, Sprite _sprite)
    {
        _image.sprite = _sprite;
        _image.SetNativeSize();
    }

    public static void FillAmount_Func(this Image _image, int _setValue, int _maxValue)
    {
        _image.FillAmount_Func((float)_setValue, (float)_maxValue);
    }
    public static void FillAmount_Func(this Image _image, float _setValue, int _maxValue)
    {
        _image.FillAmount_Func(_setValue, (float)_maxValue);
    }
    public static void FillAmount_Func(this Image _image, int _setValue, float _maxValue)
    {
        _image.FillAmount_Func((float)_setValue, _maxValue);
    }
    public static void FillAmount_Func(this Image _image, float _setValue, float _maxValue)
    {
        _image.fillAmount = _setValue / _maxValue;
    }
    #endregion
    #region Co-routine Group
    private static Dictionary<float, WaitForSeconds> dic;
    public static WaitForSeconds GetWaitForSeconds_Func(float _time)
    {
        if(Cargold_Library.dic != null) { }
        else Cargold_Library.dic = new Dictionary<float, WaitForSeconds>();

        WaitForSeconds _delay = null;
        if (Cargold_Library.dic.TryGetValue(_time, out _delay) == true) { }
        else
        {
            _delay = new WaitForSeconds(_time);
            Cargold_Library.dic.Add_Func(_time, _delay);
        }

        return _delay;
    }
    public static void WFS_GCback_Func()
    {
        // 메모리 확보를 위해 로딩 때 호출해주자

        Cargold_Library.dic.Clear();
    }

    public static float GetCalcValue_Func(float _time, float _multipleValue = 0.02f)
    {
        return _multipleValue / _time;
    }
    public static IEnumerator SetFadeIn_Cor(float _time, Image _target, float _inValue = 1f)
    {
        float _calcValue = GetCalcValue_Func(_time);

        for (float _alphaValue = 0; _alphaValue <= _inValue;)
        {
            _alphaValue += _calcValue;
            _target.SetFade_Func(_alphaValue);

            yield return new WaitForFixedUpdate();
        }
    }
    public static IEnumerator SetFadeOut_Cor(float _time, Image _target, float _outValue = 0f)
    {
        float _calcValue = GetCalcValue_Func(_time);

        for (float _alphaValue = 1f; _outValue <= _alphaValue;)
        {
            _alphaValue -= _calcValue;
            _target.SetFade_Func(_alphaValue);

            yield return new WaitForFixedUpdate();
        }
    }
    public static IEnumerator SetFade_Cor(this Graphic _graphic, float _startValue, float _endValue, float _time)
    {
        float _calcValue = GetCalcValue_Func(_time);

        if (_startValue < _endValue)
        {
            while (_startValue < _endValue)
            {
                _startValue += _calcValue;
                _graphic.SetFade_Func(_startValue);
                yield return new WaitForFixedUpdate();
            }
        }
        else if (_endValue < _startValue)
        {
            while (_endValue < _startValue)
            {
                _startValue -= _calcValue;
                _graphic.SetFade_Func(_startValue);
                yield return new WaitForFixedUpdate();
            }
        }
        else
        {
            yield break;
        }
    }
    #endregion
    #region DoTween Group
    public static void DoTweenDelay_Func()
    {

    }
    #endregion
    #region Transform Group
    public static void LootAt_Func(this Transform _lookerTrf, Vector3 _lookPos)
    {
        _lookerTrf.rotation = Cargold_Library.GetLookAt_Func(_lookerTrf.position, _lookPos);
    }
    #endregion
    #region Math Group
    public static Quaternion GetLookAt_Func(Vector3 _thisPos, Vector3 target)
    {
        Vector3 _normalTangent = (target - _thisPos).normalized;
        float angle = Mathf.Atan2(_normalTangent.y, _normalTangent.x) * Mathf.Rad2Deg;

        Quaternion rotation = new Quaternion();
        rotation.eulerAngles = new Vector3(0, 0, angle - 90);
        return rotation;
    }

    public static Vector3 GetBezier_Func(Vector3 _startPos, Vector3 _curvePos, Vector3 _arrivePos, float _time)
    {
        var omt = 1f - _time;
        return _startPos * omt * omt + 2f * _curvePos * omt * _time + _arrivePos * _time * _time;
    }

    public static Vector2 GetBezier_Func(Vector2 _startPos, Vector2 _curvePos, Vector2 _arrivePos, float _time)
    {
        var omt = 1f - _time;
        return _startPos * omt * omt + 2f * _curvePos * omt * _time + _arrivePos * _time * _time;
    }

    public static Vector2 GetCircumferencePos_Func(Vector2 _circlePos, float _radius, float _angle)
    {
        float _calcAngle = _angle * Mathf.Deg2Rad;
        float _cos = _radius * Mathf.Cos(_calcAngle);
        float _sin = _radius * Mathf.Sin(_calcAngle);

        return _circlePos += new Vector2(_cos, _sin);
    }
    #endregion
}

#region Coroutine Group
//namespace Cargold.Coroutine
//{
//    public static class Coroutine_C
//    {
//        private static Dictionary<float, WaitForSeconds> waitForSecondsDic;

//        public static Coroutine_C Instance
//        {
//            get
//            {
//                if(Coroutine_C.instance != null)
//                {
                    
//                }
//                else
//                {
//                    Coroutine_C.instance = new Coroutine_C();
//                }

//                return Coroutine_C.instance;
//            }
//        }
//        private static Coroutine_C instance;

//        private static Coroutine_C()
//        {

//        }

//        public static Coroutine_C Init_Func()
//        {
//            return new Coroutine_C();
//        }

//        public WaitForSeconds GetWaitForSeconds_Func(float _time)
//        {
//            WaitForSeconds _delay = null;

//            if (waitForSecondsDic.TryGetValue(_time, out _delay) == true)
//            {

//            }
//            else
//            {
//                _delay = new WaitForSeconds(_time);

//                waitForSecondsDic.Add_Func(_time, _delay);
//            }

//            return _delay;
//        }
//    }
//}
#endregion
#region Delegate Template (= Action, Func)
public delegate void Action_C();
public delegate void Action_C<T>(T _t);
public delegate void Action_C<T1, T2>(T1 _t1, T2 _t2);
public delegate void Action_C<T1, T2, T3>(T1 _t1, T2 _t2, T3 _t3);
public delegate void Action_C<T1, T2, T3, T4>(T1 _t1, T2 _t2, T3 _t3, T4 _t4);
public delegate void Action_C<T1, T2, T3, T4, T5>(T1 _t1, T2 _t2, T3 _t3, T4 _t4, T5 _t5);
public delegate void Action_C<T1, T2, T3, T4, T5, T6>(T1 _t1, T2 _t2, T3 _t3, T4 _t4, T5 _t5, T6 _t6);
public delegate void Action_C<T1, T2, T3, T4, T5, T6, T7>(T1 _t1, T2 _t2, T3 _t3, T4 _t4, T5 _t5, T6 _t6, T7 _t7);
public delegate void Action_C<T1, T2, T3, T4, T5, T6, T7, T8>(T1 _t1, T2 _t2, T3 _t3, T4 _t4, T5 _t5, T6 _t6, T7 _t7, T8 _t8);
public delegate void Action_C<T1, T2, T3, T4, T5, T6, T7, T8, T9>(T1 _t1, T2 _t2, T3 _t3, T4 _t4, T5 _t5, T6 _t6, T7 _t7, T8 _t8, T9 _t9);

public delegate ReturnType Func_C<ReturnType>();
public delegate ReturnType Func_C<ReturnType, T>(T _t);
public delegate ReturnType Func_C<ReturnType, T1, T2>(T1 _t1, T2 _t2);
public delegate ReturnType Func_C<ReturnType, T1, T2, T3>(T1 _t1, T2 _t2, T3 _t3);
public delegate ReturnType Func_C<ReturnType, T1, T2, T3, T4>(T1 _t1, T2 _t2, T3 _t3, T4 _t4);
public delegate ReturnType Func_C<ReturnType, T1, T2, T3, T4, T5>(T1 _t1, T2 _t2, T3 _t3, T4 _t4, T5 _t5);
public delegate ReturnType Func_C<ReturnType, T1, T2, T3, T4, T5, T6>(T1 _t1, T2 _t2, T3 _t3, T4 _t4, T5 _t5, T6 _t6);
public delegate ReturnType Func_C<ReturnType, T1, T2, T3, T4, T5, T6, T7>(T1 _t1, T2 _t2, T3 _t3, T4 _t4, T5 _t5, T6 _t6, T7 _t7);
public delegate ReturnType Func_C<ReturnType, T1, T2, T3, T4, T5, T6, T7, T8>(T1 _t1, T2 _t2, T3 _t3, T4 _t4, T5 _t5, T6 _t6, T7 _t7, T8 _t8);
public delegate ReturnType Func_C<ReturnType, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T1 _t1, T2 _t2, T3 _t3, T4 _t4, T5 _t5, T6 _t6, T7 _t7, T8 _t8, T9 _t9);
#endregion
#region Singleton
public abstract class Cargold_Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _Instance;
    public static T Instance
    {
        get
        {
            if (_Instance != null)
            {

            }
            else
            {
                Generate_Func(false);
            }

            return _Instance;
        }
    }

    // 싱글턴을 사용하기 위해 아래의 함수를 최초 1회 호출해야 함.
    // 미호출 시 Property에서 예외처리하므로 문제는 없으나 Error Log는 출력됨.
    public static void Generate_Func(bool _isNormalAccess = true)
    {
        if (_isNormalAccess == true)
        {

        }
        else
        {
            Debug.LogError("아직 생성되지 않은 싱글턴에 접근하였습니다. - " + typeof(T));
        }

        GameObject singleton = new GameObject();
        _Instance = singleton.AddComponent<T>();
        singleton.name = "(Singleton)" + typeof(T).ToString();
        DontDestroyOnLoad(singleton);

        Debug.Log("[Singleton]" + typeof(T) + " 생성, 객체이름은 " + singleton);
    }

    public abstract IEnumerator Init_Cor();
}
#endregion
#region Observer
namespace Cargold.Observer
{
    // 알림에 인자를 포함할 순 없을까? 181030

    public interface IListener_C
    {
        void Notify_Func();
    }

    public class Observer_C<TKey, TValue>
    {
        private List<IListener_C> listenerList;

        public Observer_C()
        {
            listenerList = new List<IListener_C>();
        }
        public bool AddListener_Func(IListener_C _iListener)
        {
            // 구독자 등록
            // 중복 구독자가 아닐 때만 등록

            bool _isContainListener = listenerList.Contains(_iListener);

            if (_isContainListener == false)
            {
                listenerList.Add(_iListener);
            }
            else
            {

            }

            return _isContainListener;
        }
        public bool Notify_Func()
        {
            // 등록된 모든 구독자에게 알림
            // 구독자가 있는지 확인

            if (0 < listenerList.Count)
            {
                for (int i = 0; i < listenerList.Count; i++)
                {
                    listenerList[i].Notify_Func();
                }

                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Remove_Func()
        {
            // 구독 전체 해제
            // 구독자가 있는가?

            if (0 < listenerList.Count)
            {
                listenerList.Clear();

                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Remove_Func(IListener_C _iListener)
        {
            // 특정 구독자만 해지
            // 특정 구독자가 존재하는가?

            if (this.listenerList.Contains(_iListener) == true)
            {
                this.listenerList.Remove(_iListener);

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
#endregion
#region Which One
namespace Cargold.WhichOne
{
    public sealed class WhichOne<T> where T : class, IWhichOne
    {
        private T whichOne;

        public WhichOne()
        {
            whichOne = null;
        }
        public void Selected_Func(T _whichOne)
        {
            // 인자값을 선택 개체로 등록하고 '선택'이벤트 전달.
            // 만약 기 개체를 선택한 경우 선택 개체에게 중복 선택임을 알림
            // 만약 이미 선택 개체가 있다면, 기 선택 개체에게 '선택 해제'이벤트 전달

            if (this.whichOne == null)
            {
                this.whichOne = _whichOne;

                _whichOne.Selected_Func();
            }
            else
            {
                if (this.whichOne == _whichOne)
                {
                    _whichOne.Selected_Func(true);
                }
                else
                {
                    this.whichOne.SelectCancel_Func();

                    this.whichOne = _whichOne;

                    _whichOne.Selected_Func();
                }
            }
        }
        public void SelectCancel_Func()
        {
            // 선택 해제. 선택 개체에게 선택 해제 이벤트 알림

            if (this.whichOne != null)
            {
                this.whichOne.SelectCancel_Func();

                this.whichOne = null;
            }
            else
            {

            }
        }
        public T GetWhichOne_Func()
        {
            // 선택 개체 반환

            return this.whichOne;
        }
        public bool Compare_Func(T _check)
        {
            // 인자값과 선택 개체가 동일한가?

            return this.whichOne == _check;
        }
        public bool HasWhichOne_Func()
        {
            // 선택한 개체가 있는가?

            if (this.whichOne == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public interface IWhichOne
    {
        void Selected_Func(bool _repeat = false); // 선택됨
        void SelectCancel_Func(); // 선택 해제됨
    }

    // 1. 선택 순서를 기록하고 이를 역행하면서 선택 해제하고 싶다면?
    /*
     * List를 써서 순서 기록
     * 순서를 역순으로 돌아갈 수 있음
     * List Clear 시점은 Select 값이 없을 때?
     */

    // 2. 선택 개수를 2개 이상인 경우엔?
    /*
     * 선택 개수를 초과할 경우 가장 먼저 선택된 객체가 해제?
     */
}
#endregion
#region TileManager
/*
 * 타일 기반 시스템이 갖춰야 하는 기본적인 기능을 미리 만들어보자!
 * 
 * 타일을 통제하는 매니저, 타일. 둘로 나눠서 만들어보자
 * 
 */

namespace Cargold.TileSystem
{
    // 사용법
    // 타일을 통제할 매니저는 TileSystem_Class를 상속 받아야 한다.
    // 타일 매니저에 관리 당할 일반 타일 클래스는 Tile_Class를 상속 받아야 한다.
    // 타일 매니저와 일반 타일 클래스는 모두 초기화 함수(Init_Func)을 호출해야 한다.
    // 타일 클래스는 타일 매니저에게 관리를 받기 위해선 SetTile 함수를 호출해야 한다.

    public abstract class TileSystem_Class<T> : MonoBehaviour
    {
        protected TileGroup_Class<T>[,] tileGroupClassArr;
        public int PosX_Max { get { return tileGroupClassArr.GetLength(0); } }
        public int PosY_Max { get { return tileGroupClassArr.GetLength(1); } }
        public TilePosData Pos_Max { get { return new TilePosData(PosX_Max, PosY_Max); } }

        // 타일맵 시스템의 시작위치 보정값
        public virtual Vector2 TilePos_InitData { get { return new Vector2(0, 0); } }

        // 각 타일간의 간격
        public virtual Vector2 TileSpace { get { return new Vector2(1, 1); } }

        // 초기화
        public void Init_Func(int _x, int _y)
        {
            tileGroupClassArr = new TileGroup_Class<T>[_x, _y];

            for (int x = 0; x < _x; x++)
            {
                for (int y = 0; y < _y; y++)
                {
                    tileGroupClassArr[x, y] = new TileGroup_Class<T>(x, y);
                }
            }

        }

        // 특정 타일을 매니저에게 알려서 관리 받도록 세팅
        public void SetTile_Func(Tile_Class<T> _tileClass)
        {
            int _x = _tileClass.TilePosX;
            int _y = _tileClass.TilePosY;

            if (CheckTileRange_Func(_x, _y) == true)
            {
                this.tileGroupClassArr[_x, _y].SetTile_Func(_tileClass);
            }
            else
            {
                throw new Exception("필드의 영역을 벗어났습니다.");
            }
        }
        public void RemoveTile_Func(Tile_Class<T> _tileClass)
        {
            int _x = _tileClass.TilePosX;
            int _y = _tileClass.TilePosY;

            if (CheckTileRange_Func(_x, _y) == true)
            {
                this.tileGroupClassArr[_x, _y].RemoveTile_Func(_tileClass);
            }
            else
            {
                throw new Exception("필드의 영역을 벗어났습니다.");
            }
        }

        // 타일 범위 안에 있는가?
        public bool CheckTileRange_Func(TilePosData _posData)
        {
            return CheckTileRange_Func(_posData.X, _posData.Y);
        }
        public bool CheckTileRange_Func(int _x, int _y)
        {
            if (this.PosX_Max <= _x || _x < 0)
            {
                Debug.Log("_x : " + _x);
                return false;
            }
            else if (this.PosY_Max <= _y || _y < 0)
            {
                Debug.Log("_y : " + _y);
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool GetTile_Func(TilePosData _data, T _type, out Tile_Class<T> _tileClass)
        {
            return this.GetTile_Func(_data.X, _data.Y, _type, out _tileClass);
        }
        public bool GetTile_Func(int _x, int _y, T _type, out Tile_Class<T> _tileClass)
        {
            if (CheckTileRange_Func(_x, _y) == true)
            {
                TileGroup_Class<T> _tileGroupClass = this.tileGroupClassArr[_x, _y];

                return _tileGroupClass.CheckTile_Func(_type, out _tileClass);
            }
            else
            {
                throw new Exception("필드의 영역을 벗어났습니다.");
            }
        }
        public bool[] GetTileArr_Func(TilePosData _posData, out Tile_Class<T>[] _tileClassArr, params T[] _typeArr)
        {
            return GetTileArr_Func(_posData.X, _posData.Y, out _tileClassArr, _typeArr);
        }
        public bool[] GetTileArr_Func(int _x, int _y, out Tile_Class<T>[] _tileClassArr, params T[] _typeArr)
        {
            TileGroup_Class<T> _tileGroupClass = this.tileGroupClassArr[_x, _y];

            bool[] _isReturnArr = new bool[_typeArr.Length];
            _tileClassArr = new Tile_Class<T>[_typeArr.Length];

            for (int i = 0; i < _typeArr.Length; i++)
            {
                _isReturnArr[i] = _tileGroupClass.CheckTile_Func(_typeArr[i], out _tileClassArr[i]);
            }

            return _isReturnArr;
        }

        public bool Move_Func(Tile_Class<T> _moveTileClass, int _arrivePosX, int _arrivePosY, DirectionType _moveDir = DirectionType.None, bool _isJustCheck = false, params T[] _checkTileTypeArr)
        {
            // 이동 좌표가 타일 범위를 초과했는가?
            bool _isTileRange = this.CheckTileRange_Func(_arrivePosX, _arrivePosY);

            if (_isTileRange == true)
            {
                // 이동 타일의 정보
                int _movePosX = _moveTileClass.TilePosX;
                int _movePosY = _moveTileClass.TilePosY;

                // 도착 타일의 정보
                TileGroup_Class<T> _arriveTileGroupClass = this.tileGroupClassArr[_arrivePosX, _arrivePosY];
                Tile_Class<T>[] _arriveTileClassArr = new Tile_Class<T>[_checkTileTypeArr.Length];

                bool _isMovable = false;
                bool[] _isArriveTileHaveArr = new bool[_checkTileTypeArr.Length];

                for (int i = 0; i < _checkTileTypeArr.Length; i++)
                {
                    _isArriveTileHaveArr[i] = _arriveTileGroupClass.CheckTile_Func(_checkTileTypeArr[i], out _arriveTileClassArr[i]);

                    // 도착 지점에 특정 타일이 있는가?
                    if (_isArriveTileHaveArr[i] == true)
                    {
                        // 도착 지점의 특정 타일의 이동 가능 여부 확인
                        _isMovable = _arriveTileClassArr[i].CheckMovable_Func(_moveTileClass);

                        if (_isMovable == true)
                        {

                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        _isMovable = true;
                    }
                }

                // 단순히 확인만 할 것인가?
                if (_isJustCheck == false)
                {
                    // 도착 지점으로 이동 가능한가?
                    if (_isMovable == true)
                    {
                        // 타일 그룹에서 이동 타일을 제외시킴
                        TileGroup_Class<T> _moveTileGroupClass = this.tileGroupClassArr[_movePosX, _movePosY];
                        _moveTileGroupClass.RemoveTile_Func(_moveTileClass);

                        for (int i = 0; i < _isArriveTileHaveArr.Length; i++)
                        {
                            // 도착 지점에 특정 타일이 있는가?
                            if (_isArriveTileHaveArr[i] == true)
                            {
                                // 도착 지점의 특정 타일에게 '밀려남'을 알림
                                _arriveTileClassArr[i].Pushed_Func(_moveTileClass, _moveDir);
                            }
                            else
                            {

                            }
                        }

                        _arriveTileGroupClass.SetTile_Func(_moveTileClass);
                    }
                    else
                    {

                    }
                }
                else
                {

                }

                return _isMovable;
            }
            else
            {
                return false;
            }
        }
        public TileGroup_Class<T> GetTileGroupClass_Func(TilePosData _posData)
        {
            return this.GetTileGroupClass_Func(_posData.X, _posData.Y);
        }
        public TileGroup_Class<T> GetTileGroupClass_Func(int _x, int _y)
        {
            this.CheckTileRange_Func(_x, _y);

            return this.tileGroupClassArr[_x, _y];
        }
    }
    public class TileGroup_Class<T>
    {
        private int xAxis;
        private int yAxis;

        public int XAxis { get { return xAxis; } }
        public int YAxis { get { return yAxis; } }

        private Dictionary<T, Tile_Class<T>> tileGroupDic;

        public TileGroup_Class(int _x, int _y)
        {
            tileGroupDic = new Dictionary<T, Tile_Class<T>>();

            xAxis = _x;
            yAxis = _y;
        }
        public void SetTile_Func(Tile_Class<T> _tileClass)
        {
            T _tileType = _tileClass.TileType;

            if (this.tileGroupDic.ContainsKey(_tileType) == false)
            {
                this.tileGroupDic.Add_Func(_tileType, _tileClass);
            }
            else
            {
                Debug.Log("Set : " + _tileClass.TilePosX + "_" + _tileClass.TilePosY);
                Debug.Log("Type : " + _tileType);
                throw new Exception("다음 타입의 타일을 추가하려 했으나 이미 배치되어 있습니다. : " + _tileType);
            }
        }
        public void RemoveTile_Func(Tile_Class<T> _tileClass)
        {
            //Debug.Log("Remove : " + _tileClass.PosX + "_" + _tileClass.PosY);

            T _tileType = _tileClass.TileType;

            if (this.tileGroupDic.ContainsKey(_tileType) == true)
            {
                this.tileGroupDic.Remove_Func(_tileType);
            }
            else
            {
                Debug.Log("Remove : " + _tileClass.TilePosX + "_" + _tileClass.TilePosY);
                Debug.Log("Type : " + _tileType);
                throw new Exception("다음 타입의 타일을 제거하려 했으나 배치되어 있지 않습니다. : " + _tileType);
            }
        }
        public bool CheckTile_Func(T _tileType, out Tile_Class<T> _tileClass)
        {
            return this.tileGroupDic.TryGetValue(_tileType, out _tileClass);
        }
    }
    public abstract class Tile_Class<T> : MonoBehaviour
    {
        protected TileSystem_Class<T> tileSystemClass;
        [SerializeField] protected int tilePosX;
        [SerializeField] protected int tilePosY;
        [SerializeField] protected T tileType;
        public T TileType { get { return this.tileType; } }

        public int TilePosX { get { return tilePosX; } }
        public int TilePosY { get { return tilePosY; } }
        public TilePosData TilePos { get { return new TilePosData(tilePosX, tilePosY); } }

        public virtual void Init_Func(TilePosData _tilePosData, TileSystem_Class<T> _tileSystemClass, bool _isSetTile = true)
        {
            this.Init_Func(_tilePosData.X, _tilePosData.Y, _tileSystemClass, _isSetTile);
        }
        public virtual void Init_Func(int _x, int _y, TileSystem_Class<T> _tileSystemClass, bool _isSetTile = true)
        {
            this.SetPos_Func(_x, _y);

            this.tileSystemClass = _tileSystemClass;

            this.tileType = Init_TileType_Func();

            if (_isSetTile == true) _tileSystemClass.SetTile_Func(this);
        }
        protected abstract T Init_TileType_Func();
        protected void SetPos_Func(TilePosData _posData)
        {
            this.SetPos_Func(_posData.X, _posData.Y);
        }
        protected void SetPos_Func(int _posX, int _posY)
        {
            this.tilePosX = _posX;
            this.tilePosY = _posY;
        }

        // 이동 가능 여부
        public virtual bool CheckMovable_Func(Tile_Class<T> _moveTileClass)
        {
            return false;
        }

        // 이동을 시도할 경우 호출됨
        protected bool Move_Func(DirectionType _dirType, bool _isJustCheck = false, params T[] _checkTileTypeArr)
        {
            TilePosData _arrivePosData = TilePosData.GetPos_Func(this, _dirType);

            // 이동 시 확인할 타일 종류가 정해져있는가?
            if (0 < _checkTileTypeArr.Length)
            {

            }
            else
            {
                // 위 타일과 동일한 타일 종류를 확인한다.
                _checkTileTypeArr = new T[1] { this.tileType };
            }

            bool _isMovable = tileSystemClass.Move_Func(this, _arrivePosData.X, _arrivePosData.Y, _dirType, _isJustCheck, _checkTileTypeArr);

            // 확인만 하는게 아닌가?
            if (_isJustCheck == false)
            {
                // 이동 가능한가?
                if (_isMovable == true)
                {
                    this.SetPos_Func(_arrivePosData);

                    this.Move_Func(_arrivePosData);
                }
                else
                {
                    this.MoveFail_Func();
                }
            }
            else
            {

            }

            return _isMovable;
        }

        // 이동한 경우 호출됨
        protected virtual void Move_Func(TilePosData _posData)
        {
            this.Move_Func(_posData.X, _posData.Y);
        }
        protected virtual void Move_Func(int _posX, int _posY)
        {
            float _posX_f = _posX * this.tileSystemClass.TileSpace.x;
            float _posY_f = _posY * this.tileSystemClass.TileSpace.y;

            Vector2 _initPos = this.tileSystemClass.TilePos_InitData;
            _posX_f += _initPos.x;
            _posY_f += _initPos.y;

            this.transform.position = new Vector2(_posX_f, _posY_f);
        }

        // 이동에 실패한 경우 호출됨
        protected abstract void MoveFail_Func();

        // 밀려난 경우 호출됨
        public abstract void Pushed_Func(Tile_Class<T> _pushTileClass, DirectionType _pushDir = DirectionType.None);
    }

    public enum DirectionType
    {
        Left = -2,
        Down = -1,
        None = 0,
        Up = 1,
        Right = 2,
    }

    [System.Serializable]
    public struct TilePosData
    {
        [SerializeField] private int x;
        [SerializeField] private int y;

        public TilePosData(int _x, int _y)
        {
            this.x = _x;
            this.y = _y;
        }

        public int X { get { return this.x; } }
        public int Y { get { return this.y; } }

        public static TilePosData GetPos_Func<T>(Tile_Class<T> _tileClass, DirectionType _dirType)
        {
            int _x = _tileClass.TilePosX;
            int _y = _tileClass.TilePosY;

            TilePosData _data = TilePosData.GetPos_Func(_x, _y, _dirType);

            return _data;
        }
        public static TilePosData GetPos_Func(TilePosData _posData, DirectionType _dirType)
        {
            return TilePosData.GetPos_Func(_posData.x, _posData.y, _dirType);
        }
        public static TilePosData GetPos_Func(int _x, int _y, DirectionType _dirType)
        {
            switch (_dirType)
            {
                case DirectionType.Up:
                    _y++;
                    break;
                case DirectionType.Right:
                    _x++;
                    break;
                case DirectionType.Down:
                    _y--;
                    break;
                case DirectionType.Left:
                    _x--;
                    break;
            }

            return new TilePosData(_x, _y);
        }
    }
}

// 타일 영역이 실시간으로 커지거나 작아지는 기능 추가
// 
#endregion
#region LayerSorting System
namespace Cargold.LayerSort
{
    using Cargold.TileSystem;

    public abstract class LayerSorting_Manager<T> : MonoBehaviour
    {
        public static LayerSorting_Manager<T> Instance;

        private Dictionary<T, int> layerGapDic;

        // 타일간 레이어 간격
        private int layerGap;
        protected int LayerGap { get { return layerGap; } }

        public void Init_Func()
        {
            Instance = this;

            layerGapDic = new Dictionary<T, int>();

            T[] _typeArr = Init_LayerType_Func();

            Init_TileGap_Func(_typeArr);
        }
        protected abstract T[] Init_LayerType_Func();

        // 타일간 레이어 간격값
        private void Init_TileGap_Func(params T[] _typeArr)
        {
            int _typeGap = Init_TypeGap_Func();

            for (int i = 0; i < _typeArr.Length; i++)
            {
                int _keyCount = 0;

                _keyCount = this.layerGapDic.Keys.Count;

                int _layerRangeValue = _typeGap * _keyCount;

                this.layerGapDic.Add_Func(_typeArr[i], _layerRangeValue);

                // 새로운 타입이 추가되었으므로 타일간 레이어 간격도 그만큼 확장한다.
                layerGap += _typeGap;
            }
        }
        // 타입간 레이어 간격값
        // 타입 사이에 많은 레이어 구분이 필요할 경우 재정의하여 값을 10보다 키우면 됨
        protected virtual int Init_TypeGap_Func()
        {
            return 10;
        }

        public void SetLayerSort_Func(SpriteRenderer _spriteRend, T _layerType, TilePosData _posData, int _layerExtraID = 0)
        {
            this.SetLayerSort_Func(_spriteRend, _layerType, _posData.Y, _layerExtraID);
        }
        public void SetLayerSort_Func(SpriteRenderer _spriteRend, T _layerType, Tile_Class<T> _tileClass, int _layerExtraID = 0)
        {
            this.SetLayerSort_Func(_spriteRend, _layerType, _tileClass.TilePosY, _layerExtraID);
        }
        public void SetLayerSort_Func(SpriteRenderer _spriteRend, T _layerType, int _posY, int _layerExtraID = 0)
        {
            int _typeGap = 1;
            if (this.layerGapDic.TryGetValue(_layerType, out _typeGap) == true)
            {

            }
            else
            {
                throw new Exception("다음 타입의 레이어는 초기화되지 않았습니다. : " + _layerType);
            }

            // 스프라이트의 타일 Y값만큼 타일 간격을 곱하여 레이어를 정렬한다.
            int _layerSortID = _posY * this.LayerGap;

            // 스프라이트의 타입만큼 레이어를 조금 더 정렬한다.
            _layerSortID += _typeGap;

            // 임의 레이어값만큼 레이어를 조금 더 정렬한다.
            _layerSortID += _layerExtraID;

            // 정렬값을 역전하여 Y축 값이 작을 수록 레이어가 앞에 나오도록 한다.
            _layerSortID *= -1;

            _spriteRend.sortingOrder = _layerSortID;
        }
    }
}
#endregion
#region FlexNum
// Clicker에서 쓰는 자원 표기 기능

// 1. 자릿수 표기 텍스트 임의 지정
// 2. BigNumber를 숫자마냥 사칙연산 가능하게끔
namespace Cargold.FlexNum
{
    // Data Type 더 다양하게 수용 ㄱㄱ

    /* 더하기
     * 구현 완료
     */

    /* 빼기
     * 미구현
     */

    /* 곱하기
     * Int, Float은 구현 완료
     * FlexNum과의 곱하기는 미구현
     */

    /* 나누기
     * 미구현
     */

    public sealed class FlexNum
    {
        #region Variable
        private List<int> numList;
        #endregion

        #region Constructor
        public FlexNum()
        {
            numList = new List<int>();

            numList.Add(0);
        }
        public FlexNum(int _initNum)
        {
            numList = new List<int>();

            this.Addition_Func(this, _initNum);
        }
        #endregion

        #region Set Property
        public static implicit operator FlexNum(int _addNum)
        {
            FlexNum _thisFnum = new FlexNum(_addNum);

            return _thisFnum;
        }
        public static explicit operator string(FlexNum _thisFnum)
        {
            string _print = "";

            int _digit = _thisFnum.Digit;

            _print = _thisFnum.GetLastDigitValue_Func().ToString();

            _print += 1 < _digit ? GetDigitText_Func(_digit) : "";

            return _print;
        }
        #endregion
        #region Get Property
        public int Digit
        {
            get
            {
                return numList.Count;
            }
        }
        public int GetLastDigitValue_Func()
        {
            int _digit = this.numList.Count;

            _digit--;

            return numList[_digit];
        }
        public int GetValue_Func(int _digit)
        {
            if (_digit <= this.Digit)
            {
                return numList[_digit - 1];
            }
            else
            {
                Debug.Log("자리수를 초과하여 요청함. 마지막 자리수의 숫자로 대신 반환하겠음");

                return this.GetLastDigitValue_Func();
            }
        }
        public string GetValueTotal_Func(bool _isHaveDigit = true)
        {
            string _print = "";

            int _digit = this.Digit;
            _print = this.GetValue_Func(_digit).ToString();

            int _reverseDigit = _digit - 1;
            while (0 < _reverseDigit)
            {
                int _getValue = this.GetValue_Func(_reverseDigit);

                _print += _isHaveDigit ? "," : "";

                if (100 <= _getValue)
                {

                }
                else if (10 <= _getValue)
                {
                    _print += "0";
                }
                else
                {
                    _print += "00";
                }

                _print += _getValue;

                _reverseDigit--;
            }

            return _print;
        }
        #endregion

        #region Addition Group
        public static FlexNum operator +(FlexNum _thisFnum, FlexNum _addFnum)
        {
            _thisFnum.Addition_Func(_thisFnum, _addFnum);

            return _thisFnum;
        }
        public static FlexNum operator +(FlexNum _thisFnum, int _addNum)
        {
            _thisFnum.Addition_Func(_thisFnum, _addNum);

            return _thisFnum;
        }
        public static FlexNum operator +(FlexNum _thisFnum, float _addNum)
        {
            Debug.Log("Float Type의 실수값은 모두 소실됩니다.");

            _thisFnum.Addition_Func(_thisFnum, (int)_addNum);

            return _thisFnum;
        }
        private void Addition_Func(FlexNum _thisFnum, FlexNum _addFnum)
        {
            int _operDigit = _addFnum.Digit;

            // 첫번째 자리수부터 마지막 자리수까지 순차적으로 계산
            for (int i = 1; i < _operDigit + 1; i++)
            {
                int _value = _addFnum.GetValue_Func(i);

                _thisFnum.Addition_Func(_value, i);
            }
        }
        private void Addition_Func(FlexNum _thisFnum, int _value)
        {
            int _digit = 0;

            while (0 < _value)
            {
                _digit++;

                int _insertNum = 0;

                // 숫자가 1천 이상인가요
                if (1000 <= _value)
                {
                    // 삽입할 값 저장 (입력값의 1천 미만의 수를 삽입값에 저장)
                    _insertNum = _value % 1000;

                    // 입력값에 삽입값을 제거한 후 자리수 한 칸(3자리) 낮추기
                    _value -= _insertNum;
                    _value /= 1000;
                }
                else
                {
                    // 숫자를 삽입값에 저장
                    _insertNum = _value;

                    // 입력값은 제거
                    _value = 0;
                }

                _thisFnum.Addition_Func(_insertNum, _digit);
            }
        }
        private void Addition_Func(int _value, int _digit)
        {
            // 더하려는 자리수가 부족한 만큼 추가
            while (numList.Count < _digit)
            {
                numList.Add(0);
            }

            // List Index와 List.Count의 차이로 인한 -1
            _digit--;

            numList[_digit] += _value;

            // 자리수에 들어간 값이 1천 미만인가?
            if (numList[_digit] < 1000)
            {
                // 정상
            }
            // 자리수에 들어간 값이 1천 이상이거나 100만 미만인가?
            else if (numList[_digit] < 1000000)
            {
                int _insertNum = (int)(numList[_digit] * 0.001f);
                numList[_digit] -= _insertNum * 1000;

                // 다음 자리수가 할당되어 있는가?
                if (_digit + 1 < numList.Count)
                {
                    numList[_digit + 1] += _insertNum;
                }
                else
                {
                    numList.Add(_insertNum);
                }
            }
            else
            {
                throw new Exception("자리수가 올라가는 값이 두 단계를 건너 뜀");
            }
        }
        #endregion
        #region Subtraction Group
        public static FlexNum operator -(FlexNum _thisFnum, FlexNum _subFnum)
        {
            throw new Exception("미구현");

            //_thisFnum.Operator_Func(_thisFnum, _subFnum, OperatorType.Subtraction);

            return _thisFnum;
        }
        public static FlexNum operator -(FlexNum _thisFnum, int _subNum)
        {
            throw new Exception("미구현");

            //_thisFnum.Operator_Func(_thisFnum, _subNum, OperatorType.Subtraction);

            return _thisFnum;
        }
        private void Subtraction_Func(int _subNum)
        {
            //int _digit = 0;

            //while (0 < _subNum)
            //{
            //    _digit++;

            //    int _insertNum = 0;
            //    if (1000 <= _subNum)
            //    {
            //        _insertNum = _subNum % 1000;

            //        _subNum -= _insertNum;
            //        _subNum /= 1000;
            //    }
            //    else
            //    {
            //        _insertNum = _subNum;

            //        _subNum = 0;
            //    }

            //    this.Addition_Func(_insertNum, _digit);
            //}
        }
        #endregion
        #region Multiplication Group
        public static FlexNum operator *(FlexNum _thisFnum, FlexNum _multiFnum)
        {
            throw new Exception("미구현");

            //_thisFnum.Multiplication_Func(_thisFnum, _multiFnum);

            return _thisFnum;
        }
        public static FlexNum operator *(FlexNum _thisFnum, int _multipleNum)
        {
            _thisFnum.Multiplication_Func(_thisFnum, (float)_multipleNum);

            return _thisFnum;
        }
        public static FlexNum operator *(FlexNum _thisFnum, float _multipleNum)
        {
            _thisFnum.Multiplication_Func(_thisFnum, _multipleNum);

            return _thisFnum;
        }
        private void Multiplication_Func(FlexNum _thisFnum, FlexNum _multiFnum)
        {
            //int _multiDigit = _multiFnum.Digit;

            //// 첫번째 자리수부터 마지막 자리수까지 순차적으로 계산
            //for (int i = 1; i <= _multiDigit; i++)
            //{
            //    float _value = _multiFnum.GetValue_Func(i);

            //    _thisFnum.Multiplication_Func(_thisFnum, _value, i);
            //}
        }
        private void Multiplication_Func(FlexNum _thisFnum, float _multipleNum, int _multipleDigit)
        {
            //for (int _digit = _thisFnum.Digit; 0 < _digit; _digit--)
            //{
            //    int _num = _thisFnum.numList[_digit - 1];

            //    _thisFnum.numList[_digit - 1] = 0;

            //    _num = (_num * _multipleNum).ToInt();

            //    // _multipleDigit 이녀석이 2 이상이면 1 깎음
            //    //_multipleDigit -= 1 < _multipleDigit ? 1 : 0;

            //    this.Addition_Func(_num, _digit * _multipleDigit);
            //}
        }
        private void Multiplication_Func(FlexNum _thisFnum, float _multipleNum)
        {
            for (int _digit = _thisFnum.Digit; 0 < _digit; _digit--)
            {
                int _num = numList[_digit - 1];

                numList[_digit - 1] = 0;

                _num = (_num * _multipleNum).ToInt();

                this.Addition_Func(_num, _digit);
            }
        }
        #endregion
        #region Division Group
        public static FlexNum operator /(FlexNum _thisFnum, FlexNum _minusFnum)
        {
            throw new Exception("미구현");
        }
        public static FlexNum operator /(FlexNum _thisFnum, int _minusNum)
        {
            throw new Exception("미구현");
        }
        #endregion

        #region Private Property
        private static string GetDigitText_Func(int _digit)
        {
            KeyCode _asciiCode = (KeyCode)(95 + _digit);
            string _returnText = _asciiCode.ToString();

            return _returnText;
        }
        #endregion

        public enum OperatorType
        {
            None = -1,
            Addition,
            Subtraction,
            Multiplication,
            Division,
        }
    }
}
#endregion
#region ResourceFindPath
namespace Cargold.ResourceFindPath
{
    // ResourceFindPath 상속 받는 클래스에서 경로를 스크립트에 적어놓고 쓰는 거 추천

    public abstract class ResourceFindPath<PathType> : MonoBehaviour
    {
        private Dictionary<PathType, string> pathDic;

        public virtual void Init_Func()
        {
            pathDic = new Dictionary<PathType, string>();
        }

        public void SetPath_Func(PathType _pathType, string _path)
        {
            pathDic.Add_Func(_pathType, _path);
        }

        public T GetResource_Func<T>(PathType _pathType, bool _isDebug = true) where T : UnityEngine.Object
        {
            string _path = this.pathDic.GetValue_Func(_pathType);

            return this.GetResource_Func<T>(_path, _isDebug);
        }

        // 잘 불러와졌는지 체크
        public T GetResource_Func<T>(string _path, bool _isDebug = true) where T : UnityEngine.Object
        {
            T _returnObj = Resources.Load<T>(_path);
            if (_returnObj == null)
            {
                if (_isDebug == true)
                {
                    Debug.LogError("Bug : 데이터 로드 실패");
                    Debug.Log("Path : " + _path);
                }
            }

            return _returnObj;
        }
        public T[] GetResourceAll_Func<T>(PathType _pathType, bool _isDebug = true) where T : UnityEngine.Object
        {
            string _path = this.pathDic.GetValue_Func(_pathType);

            return this.GetResourceAll_Func<T>(_path, _isDebug);
        }

        // 잘 불러와졌는지 체크
        public T[] GetResourceAll_Func<T>(string _path, bool _isDebug = true) where T : UnityEngine.Object
        {
            T[] _returnObjArr = Resources.LoadAll<T>(_path);
            if (_returnObjArr == null)
            {
                if (_isDebug == true)
                {
                    Debug.LogError("Bug : 데이터 로드 실패");
                    Debug.Log("Path : " + _path);
                }
            }

            return _returnObjArr;
        }

        public ComponentType GetComponentByInstantiateObj_Func<ComponentType>(PathType _pathType)
        {
            GameObject _loadObj = this.GetResource_Func<GameObject>(_pathType);
            GameObject _genObj = GameObject.Instantiate(_loadObj);
            
            ComponentType _componentType = _genObj.GetComponent<ComponentType>();

            return _componentType;
        }
    }
}

#endregion
#region Animation_Class
public class Animation_Class : MonoBehaviour
{
    public Animation anim;
    public bool isActive;

    public IEnumerator Init_Cor()
    {
        anim.Play_Func("Appear", true, true);

        yield break;
    }
    public void Activation_Func()
    {
        anim.Play_Func("Appear");
    }
    public void Deactivation_Func()
    {
        anim.Play_Func("Appear", true);
    }
    public void CallAni_Func(string _key)
    {
        if (_key == "Appear_Start")
        {
            if (isActive == true)
            {
                isActive = false;
            }
            else
            {

            }
        }
        else if (_key == "Appear_End")
        {
            if (isActive == false)
            {
                isActive = true;
            }
            else
            {

            }
        }
        else
        {

        }
    }
}
#endregion
#region Data Structure
namespace Cargold.DataStructure
{
    public sealed class CirculateQueue<T>
    {
        private List<T> circulateList;
        private int circulateID;
        //public T GetItem { get { return 0 <= circulateID ? this.circulateList[this.circulateID] : this.circulateList[0]; } }
        public T GetItem { get { return this.circulateList[this.circulateID]; } }

        public CirculateQueue()
        {
            circulateList = new List<T>();

            circulateID = 0;
        }

        public void SetID_Func(int _id)
        {
            this.circulateID = _id;
        }

        public void Enqueue_Func(T _t)
        {
            circulateList.AddNewItem_Func(_t);
        }

        public void Enqueue_Func(params T[] _tArr)
        {
            for (int i = 0; i < _tArr.Length; i++)
            {
                circulateList.AddNewItem_Func(_tArr[i]);
            }
        }

        public T Dequeue_Func(bool _isReverse = false)
        {
            if (_isReverse == false)
            {
                circulateID++;

                if (circulateID < circulateList.Count)
                {

                }
                else
                {
                    circulateID = 0;
                }
            }
            else
            {
                circulateID--;

                if (0 <= circulateID)
                {

                }
                else
                {
                    circulateID = circulateList.Count - 1;
                }
            }

            return circulateList[circulateID];
        }
    }

    public sealed class Queue_C<T>
    {
        private List<T> queueList;
        public int QueueItemNum { get { return this.queueList.Count; } }

        public Queue_C()
        {
            this.queueList = new List<T>();
        }

        public void Enqueue_Func(T _t)
        {
            this.queueList.AddNewItem_Func(_t);
        }
        public T Dequeue_Func()
        {
            T _returnValue = queueList[0];

            this.queueList.Remove(_returnValue);

            return _returnValue;
        }
        public bool Dequeue_Func(out T _tryGet)
        {
            bool _isHave = false;

            if (0 < queueList.Count)
            {
                _isHave = true;

                _tryGet = this.Dequeue_Func();
            }
            else
            {
                _isHave = false;

                _tryGet = default(T);
            }

            return _isHave;
        }
    }
}
#endregion

// Not Complete
#region Dragger
// Potion 게임에서 쓰던 SelectMatter를 범용적으로 모듈화하여 WhichOne처럼 쓸모있게 만들자
// 1. 끌고 다니는게, 선택한 객체 그 자체일 수도 있고, 새로운 드래깅 객체일 수도 있고 ㅇㅇ
// 2. 드래그의 동기화 속도를 조절 가능하게끔
#endregion
#region TagDictionary
/*
 * 개체 하나에 태그를 여러개 지정할 경우
 * 딕셔너리에 태그를 Key로 써서 개체를 찾는 기능
 * 고로 한 개체가 여러개의 딕셔너리에 등록됨
 * 
 * 개체에 한 태그만 제거할 경우엔 관련 딕셔너리에서 Remove하면 된다.
 * 하지만 개체가 제거될 경우 개체의 태그와 관련된 모든 딕셔너리에 접근해서 제거해야 한다.
 */
#endregion
#region ObjectPool
#endregion
#region WaitForSeconds 
/*
 * WaitForSeconds가 매번 New로 생성되므로 딕셔너리에 담아서 재탕해보자
 */
#endregion
#region CoinCurve_Manager
namespace Cargold.CurveSystem
{
    using Cargold.DataStructure;
    using UnityEngine;

    public abstract class CoinCurve_Manager : MonoBehaviour
    {
        public static CoinCurve_Manager Instance;

        [SerializeField] private GameObject curveObj;

        private Transform curveObjGroupTrf;
        private Transform curveGroupTrf;
        private Transform curvePointTrf;
        private List<Transform> curveTrfList;

        protected virtual float CurveTime_min { get { return 1.5f; } }
        protected virtual float CurveTime_max { get { return 2.0f; } }
        protected virtual float PushPower_min { get { return 500f; } }
        protected virtual float PushPower_max { get { return 800f; } }

        private struct CurveInitData
        {
            private float curveTime;
            private float curveCalcTime;
            private float pushPower;

            public float CurveTime { get { return curveTime; } }
            public float CurveCalcTime { get { return curveCalcTime; } }
            public float PushPower { get { return pushPower; } }

            public void Init_Func(CoinCurve_Manager _managerClass)
            {
                curveTime = Random.Range(_managerClass.CurveTime_min, _managerClass.CurveTime_max);
                curveCalcTime = Cargold_Library.GetCalcValue_Func(curveTime);
                pushPower = Random.Range(_managerClass.PushPower_min, _managerClass.PushPower_max);
            }
        }
        CirculateQueue<CurveInitData> circulateQueue;

        private int RandNum { get { return 10; } }

        public void Init_Func()
        {
            Instance = this;

            circulateQueue = new CirculateQueue<CurveInitData>();
            for (int i = 0; i < RandNum; i++)
            {
                CurveInitData _curveData = new CurveInitData();
                _curveData.Init_Func(this);

                circulateQueue.Enqueue_Func(_curveData);
            }

            curveObjGroupTrf = new GameObject().transform;
            curveObjGroupTrf.SetParent(this.transform);

            curveGroupTrf = new GameObject().transform;
            curveGroupTrf.SetParent(this.transform);
            curvePointTrf = new GameObject().transform;
            curvePointTrf.SetParent(curveGroupTrf);

            curveTrfList = new List<Transform>();
        }

        public void OnCurve_Func(Vector2 _startPos, Transform _arriveTrf, Sprite _curveSprite, int _curveNum = 1, Action_C _del = null)
        {
            OnCurve_Func(_startPos, _arriveTrf.position, _curveSprite, _curveNum, _del);
        }
        public void OnCurve_Func(Vector2 _startPos, Vector2 _arrviePos, Sprite _curveSprite, int _curveNum = 1, Action_C _del = null)
        {
            float _curveTimeMax = 0f;
            curveGroupTrf.position = _startPos;

            for (int i = 0; i < _curveNum; i++)
            {
                CurveInitData _curveData = circulateQueue.Dequeue_Func();

                Vector3 _curvePos = this.GetCurvePos_Func(_curveData.PushPower);

                StartCoroutine(Curve_Cor(_startPos, _curvePos, _arrviePos, _curveData.CurveCalcTime, _curveSprite));

                if (_curveTimeMax < _curveData.CurveTime)
                {
                    _curveTimeMax = _curveData.CurveTime;
                }
            }

            if (_del == null)
            {

            }
            else
            {
                StartCoroutine(SetValue_Func(_curveTimeMax, _del));
            }
        }
        private Vector3 GetCurvePos_Func(float _pushPower, float _curveAngle_Min = 0f, float _curveAngle_Max = 360f)
        {
            curvePointTrf.localPosition = Vector2.zero;
            curvePointTrf.localEulerAngles = Vector3.forward * Random.Range(_curveAngle_Min, _curveAngle_Max);
            curvePointTrf.Translate(Vector3.one * _pushPower);
            Vector3 _curvePos = curvePointTrf.position;
            return _curvePos;
        }

        IEnumerator Curve_Cor(Vector2 _startPos, Vector2 _curvePos, Vector2 _arrivePos, float _curveCalcTime, Sprite _curveSprite)
        {
            Transform _coinTrf = null;
            if (curveTrfList.Count == 0)
            {
                _coinTrf = GameObject.Instantiate(curveObj).transform;
            }
            else
            {
                _coinTrf = curveTrfList[0];
                _coinTrf.gameObject.SetActive(true);

                curveTrfList.RemoveAt(0);
            }
            _coinTrf.SetParent(curveObjGroupTrf);
            _coinTrf.localPosition = _startPos;
            _coinTrf.localScale = Vector3.one;

            Curve_Script _curveClass = _coinTrf.GetComponent<Curve_Script>();
            _curveClass.Init_Func(_curveSprite);

            float _time = 0f;
            while (_time < 1f)
            {
                _time += _curveCalcTime;

                if (1f < _time)
                {
                    _time = 1f;
                }

                Vector2 _movePos = Cargold_Library.GetBezier_Func(_startPos, _curvePos, _arrivePos, _time);

                _coinTrf.transform.position = _movePos;

                yield return new WaitForFixedUpdate();
            }

            _coinTrf.localPosition = Vector2.zero;
            _coinTrf.gameObject.SetActive(false);

            curveTrfList.Add(_coinTrf);
        }
        IEnumerator SetValue_Func(float _time, Action_C _del)
        {
            yield return new WaitForSeconds(_time);

            _del();
        }

        //public void OnCurve_Func(Transform _curveTrf, Vector2 _arrivePos, float _curveSpeed = 1f, bool _isCurveOver = false, float _curveAngle_Min = 0f, float _curveAngle_Max = 360f)
        //{
        //    int _curveCalcTimeArrLength = curveCalcTimeArr.Length;
        //    int _curveTimeRandID = Random.Range(0, _curveCalcTimeArrLength);
        //    _curveSpeed = curveCalcTimeArr[_curveTimeRandID] * _curveSpeed;
        //    StartCoroutine(Curve_Cor(_curveTrf, _arrivePos, _curveSpeed, _isCurveOver, _curveAngle_Min, _curveAngle_Max));
        //}
        //IEnumerator Curve_Cor(Transform _curveTrf, Vector2 _arrivePos, float _curveTime, bool _isCurveOver, float _curveAngle_Min = 0f, float _curveAngle_Max = 360f)
        //{
        //    Vector2 _startPos = _curveTrf.position;
        //    curveGroupTrf.position = _startPos;
        //    Vector2 _curvePos = this.GetCurvePos_Func(_curveAngle_Min, _curveAngle_Max);

        //    float _time = 0f;
        //    while (true)
        //    {
        //        if (_curveTrf != null)
        //        {
        //            _time += _curveTime;

        //            if (1f < _time && _isCurveOver == false)
        //            {
        //                _time = 1f;
        //            }

        //            Vector2 _movePos = Cargold_Library.GetBezier_Func(_startPos, _curvePos, _arrivePos, _time);

        //            _curveTrf.transform.position = _movePos;

        //            yield return new WaitForFixedUpdate();
        //        }
        //        else
        //        {
        //            yield break;
        //        }
        //    }
        //}
    }

    public abstract class Curve_Script : MonoBehaviour
    {
        public abstract void Init_Func(Sprite _curveSprite);
    }
}
#endregion
#region TextPrint_Manager
// 여기서 본 내용을 넣고, 상속 받아서 프로젝트 별로 개별화 가능하게끔 하자
#endregion
#region Sound System
#endregion
#region Trash Group
namespace Cargold.Trash
{
    public class RaritySort
    {
        // 임의로 명명한 등급 순으로 영웅을 정렬하고 싶을 때 어떻게 하는가?

        public string[] fixRarityArr = { "SSS", "SS", "S", "AAA", "A", "B", "C", "D" };
        public List<hero_dic_info> hero_Dic_Info_Item;

        public class hero_dic_info
        {
            public string rarity;

            public hero_dic_info(string _rarity)
            {
                this.rarity = _rarity;
            }
        }

        private void Start()
        {
            hero_Dic_Info_Item = new List<hero_dic_info>();

            this.ReadCsv_Func(this.hero_Dic_Info_Item);

            this.Sort_Func(this.hero_Dic_Info_Item);

            this.PrintDesc_Func(this.hero_Dic_Info_Item);
        }

        private void ReadCsv_Func(List<hero_dic_info> _setList)
        {
            for (int i = 0; i < 10; i++)
            {
                int _randRarityID = UnityEngine.Random.Range(0, fixRarityArr.Length);
                string _randRarity = fixRarityArr[_randRarityID];
                hero_dic_info _info = new hero_dic_info(_randRarity);
                _setList.Add(_info);
            }
        }

        private void Sort_Func(List<hero_dic_info> _sortList)
        {
            for (int x = 0; x < _sortList.Count - 1; x++)
            {
                for (int y = x + 1; y < _sortList.Count; y++)
                {
                    hero_dic_info _x = _sortList[x];
                    hero_dic_info _y = _sortList[y];

                    int _xRarityID = this.GetRarityID_Func(_x);
                    int _yRarityID = this.GetRarityID_Func(_y);

                    if (_yRarityID < _xRarityID)
                    {
                        this.SwapHero_Func(ref _x, ref _y);

                        _sortList[x] = _x;
                        _sortList[y] = _y;

                        continue;
                    }
                    else
                    {

                    }
                }
            }
        }

        private int GetRarityID_Func(hero_dic_info _heroInfoClass)
        {
            for (int _rarity = 0; _rarity < this.fixRarityArr.Length; _rarity++)
            {
                if (_heroInfoClass.rarity != this.fixRarityArr[_rarity])
                {

                }
                else
                {
                    return _rarity;
                }
            }

            throw new Exception("해당 등급이 없다능");
        }

        private void SwapHero_Func(ref hero_dic_info _x, ref hero_dic_info _y)
        {
            hero_dic_info _temp = _x;

            _x = _y;

            _y = _temp;
        }

        private void PrintDesc_Func(List<hero_dic_info> _printList)
        {
            for (int i = 0; i < _printList.Count; i++)
            {
                Debug.Log(i + " / " + _printList[i].rarity);
            }
        }
    }
}
#endregion