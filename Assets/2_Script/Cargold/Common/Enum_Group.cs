﻿public enum DirectionType
{
    Up = -2,
    Left = -1,
    None = 0,
    Right = 1,
    Down = 2,
}

public enum EnemyType
{
    None = -1,
    Normal,
    Speed,
    Tank,
    Range,
}

public enum ResourceLoadType
{
    DatabaseSystem,
    PlayerSystem,
    LobbySytem,
    CameraSystem,

    StageSystemGroup,
}


public enum TurretType
{
    None = -1,
    Rifle = 0,
    Missile,
    Laser
}

public enum PlatureType
{
    None = -1,
    Shield,
    Mace,
    Cannon,
}