﻿public interface IGameState
{
    void StageEnter_Func();
    void StageFinish_Func();
    void LobbyEnter_Func();
}