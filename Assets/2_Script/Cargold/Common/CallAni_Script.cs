﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.Observer;

public class CallAni_Script : MonoBehaviour
{
    public GameObject[] listenerObjArr;
    public ICallAni[] iCallAniArr;

    private void Awake()
    {
        iCallAniArr = new ICallAni[listenerObjArr.Length];

        for (int i = 0; i < listenerObjArr.Length; i++)
        {
            ICallAni _iCallAni = listenerObjArr[i].GetComponent<ICallAni>();
            iCallAniArr[i] = _iCallAni;
        }
    }

    public void CallAni_Func(string _key)
    {
        for (int i = 0; i < iCallAniArr.Length; i++)
        {
            iCallAniArr[i].CallAni_Func(_key);
        }
    }
}

public interface ICallAni
{
    void CallAni_Func(string _key);
}