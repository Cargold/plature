﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallTrigger_Script : MonoBehaviour
{
    [SerializeField]
    private GameObject[] calledObjArr;
    private ICallTrigger[] iCallTriggerArr;

    private void Awake()
    {
        int _calledObjNum = calledObjArr.Length;
        iCallTriggerArr = new ICallTrigger[_calledObjNum];

        for (int i = 0; i < _calledObjNum; i++)
        {
            iCallTriggerArr[i] = calledObjArr[i].GetComponent<ICallTrigger>();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        for (int i = 0; i < iCallTriggerArr.Length; i++)
        {
            iCallTriggerArr[i].CallTrigger_Func(collision);
        }
    }
}

public interface ICallTrigger
{
    void CallTrigger_Func(Collider2D collision);
}