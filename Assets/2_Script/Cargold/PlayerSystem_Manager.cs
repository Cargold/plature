﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold.WhichOne;

public class PlayerSystem_Manager : MonoBehaviour, ISelectedPlature
{
    public static PlayerSystem_Manager Instance;

    private Plature_Script[] platureClassArr;

    private WhichOne<Plature_Script> whichOneClass;

	public void Init_Func()
	{
        Instance = this;

        // Cargold : 원래는 플레이어가 지금까지 구매한 플래쳐의 수
        int _platureHave = DataBase_Manager.Instance.PlatureDataNum;
        platureClassArr = new Plature_Script[_platureHave];
        Plature_Data[] _platureDataArr = DataBase_Manager.Instance.GetPlatureData_Func();

        for (int i = 0; i < platureClassArr.Length; i++)
        {
            // Cargold : 원래는 레벨을 불러와야 함
            platureClassArr[i] = new Plature_Script(_platureDataArr[i], 1);
        }
        
        whichOneClass = new WhichOne<Plature_Script>();

        // Cargold : 원래는 선택한 플래쳐 정보를 불러와야 함
        whichOneClass.Selected_Func(platureClassArr[0]);
    }

    public Plature_Script GetSelectedPlature_Func()
    {
        return whichOneClass.GetWhichOne_Func();
    }
}
